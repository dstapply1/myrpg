package myrpg.entities;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.Timer;

import myrpg.game.Game;

public class UnitStatusChange {
	private String text;
	private StatusChangeType type;
	private Unit unit;
	private int startingY;
	private int currentY;
	
	public UnitStatusChange(Unit unit, String text, StatusChangeType type) {
		this.unit = unit;
		this.text = text;
		this.type = type;
		startingY = unit.getY() - unit.getHeight()/3;
		currentY = startingY;
	}
	
	public void displayStatusChange(Graphics g) {
		Color color = Color.black;
		int size = 20;
		switch (type) {
		case exp:
			color = Color.magenta;
			break;
		case damage:
			color = Color.red;
			break;
		case heal:
			color = Color.green;
			break;
		case levelup:
			color = Color.yellow;
			size = 30;
			Game.drawString(g, text, unit.getX() - 45, currentY -= 2, size, color);
			return;
		case none:
			break;
		case resource:
			break;
		case stat:
			break;
		default:
			color = Color.white;
			break;
		}
		Game.drawString(g, text, unit.getX(), currentY -= 2, size, color);
	}
	
	public int getStartingY() {
		return startingY;
	}
	
	public int getCurrentY() {
		return currentY;
	}
}
