package myrpg.entities;

public enum StatusChangeType {
	none,
	damage,
	heal,
	resource,
	exp,
	stat,
	levelup,
	castInterrupted,
	castFinished
}
