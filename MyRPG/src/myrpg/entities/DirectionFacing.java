package myrpg.entities;

public enum DirectionFacing {
	forward,
	back,
	right,
	left
}
