package myrpg.entities;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.Timer;

import myrpg.creatures.Creature;
import myrpg.items.Item;
import myrpg.items.StatType;
import myrpg.spells.Spell;
import myrpg.spells.SpellCostType;
import myrpg.spells.SpellDamageType;
import myrpg.spells.SpellDummy;
import myrpg.spells.SpellEffect;
import myrpg.sprites.Sprite;
import myrpg.sprites.SpriteType;
import myrpg.view.MyRPGFrame;

public class Unit extends Entity {
	
	protected Unit self =  this;
	protected String name;
	protected boolean gravityActive;
	protected boolean walking, jumping, casting, shooting;
	// used for interruption checking
	protected boolean moving;
	protected boolean alive;
	protected int curHealth, maxHealth, resource, maxResource, armor;
	protected int level;
	protected int rank;
	protected float healthRegeneration, resourceRegeneration;
	protected SpellCostType resourceType;
	protected HashMap<StatType, Integer> baseStats;
	protected HashMap<StatType, Integer> itemStats;
	protected DirectionFacing facing;
	protected ArrayList<SpellDamageType> weaknesses;
	protected ArrayList<SpellDamageType> strengths;
	protected HashMap<String, Spell> spellBook;
	protected ArrayList<SpellDummy> spellDummies;
	protected ArrayList<UnitStatusChange> changes;
	protected Spell spellCasting;
	
	protected Timer castTimer;
	protected Timer updateTimer;
	
	protected int UNIT_SPEED = 10;
	protected int JUMPING_SPEED = -30;
	public final float GRAVITY_WEIGHT = 2.0f;
	
	public Unit(String spriteDirPath) {
		super();
		initUnit();
		this.sprite = new Sprite(spriteDirPath, this, SpriteType.unit);
		updateDimensions();
	}
	
	public Unit(Sprite sprite) {
		super();
		initUnit();
		this.sprite = new Sprite(sprite, this, SpriteType.unit);
		updateDimensions();
	}
	
	public Unit() {
		super();
		initUnit();
	}
	
	private void initUnit() {
		name = "";
		visible = true;
		gravityActive = true;
		curHealth = 100;
		maxHealth = 100;
		resource = 100;
		maxResource = 100;
		resourceType = SpellCostType.none;
		healthRegeneration = 0;
		resourceRegeneration = 0;
		level = 1;
		rank = 0;
		armor = 0;
		baseStats = new HashMap<StatType, Integer>();
		for (StatType type : StatType.values()) {
			baseStats.put(type, 0);
		}
		itemStats = new HashMap<StatType, Integer>();
		for (StatType type : StatType.values()) {
			itemStats.put(type, 0);
		}
		facing = DirectionFacing.forward;
		jumping = false;
		walking = false;
		casting = false;
		shooting = false;
		moving = false;
		alive = true;
		area = 0;
		weaknesses = new ArrayList<SpellDamageType>();
		strengths = new ArrayList<SpellDamageType>();
		spellBook = new HashMap<String, Spell>();
		spellDummies = new ArrayList<SpellDummy>();
		changes = new ArrayList<UnitStatusChange>();
		updateTimer = new Timer(1000, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				applyRegenEffects();
			}
		});
		updateTimer.start();
	}

	public int getCurHealth() {
		return curHealth;
	}
	
	public void setCurHealth(int val) {
		if (val > maxHealth) 
			curHealth = maxHealth;
		else if (val < 0)
			curHealth = 0;
		else
			curHealth = val;
	}
	
	public int getMaxHealth() {
		return maxHealth;
	}
	
	public void setMaxHealth(int val) {
		maxHealth = val;
	}
	
	public int getResourceValue() {
		return resource;
	}
	
	public void setResourceValue(int val) {
		if (resourceType == SpellCostType.health) {
			takeDamage(val);
		} else {
			if (val > maxResource)
				resource = maxResource;
			else if (val < 0)
				resource = 0;
			else
				resource = val;
		}
	}
	
	public void setMaxResourceValue(int val) {
		maxResource = val;
	}
	
	public int getMaxResourceValue() {
		return maxResource;
	}
	
	public SpellCostType getResourceType() {
		return resourceType;
	}
	
	public void setResourceType(SpellCostType type) {
		resourceType = type;
		
		switch (resourceType) {
		case energy:
			resourceRegeneration = 20;
			break;
		case mana:
			resourceRegeneration = 5;
			// TODO: apply item stat
			break;
		case none:
			break;
		default:
			break;
		
		}
	}
	
	public int getArmor() {
		return armor;
	}
	
	public void setArmor(int val) {
		armor = val;
	}
	
	public HashMap<StatType, Integer> getBaseStats() {
		return baseStats;
	}
	
	public HashMap<StatType, Integer> getItemStats() {
		return itemStats;
	}
	
	public HashMap<StatType, Integer> getAllStats() {
		HashMap<StatType, Integer> allStats = new HashMap<StatType, Integer>();
		for (StatType type : StatType.values()) {
			allStats.put(type, itemStats.get(type) + baseStats.get(type));
		}
		return allStats;
	}
	
	public boolean isGravityActive() {
		return gravityActive;
	}
	
	public void setGravityActive(boolean val) {
		gravityActive = val;
	}
	
	public DirectionFacing getDirectionFacing() {
		return facing;
	}
	
	public void setDirectionFacing(DirectionFacing val) {
		facing = val;
	}
	
	public ArrayList<SpellDamageType> getWeaknesses() {
		return weaknesses;
	}

	public void setWeaknesses(ArrayList<SpellDamageType> weaknesses) {
		this.weaknesses = weaknesses;
	}

	public ArrayList<SpellDamageType> getStrengths() {
		return strengths;
	}

	public void setStrengths(ArrayList<SpellDamageType> strengths) {
		this.strengths = strengths;
	}
	
	public boolean isAlive() {
		return alive;
	}
	
	public int getLevel() {
		return level;
	}
	
	public void setLevel(int level) {
		this.level = level;
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public boolean isJumping() {
		return jumping;
	}
	
	public void setJumping(boolean val) {
		jumping = val;
	}
	
	public HashMap<String, Spell> getSpellBook() {
		return spellBook;
	}
	
	public ArrayList<SpellDummy> getSpellDummies() {
		return spellDummies;
	}
	
	public Spell getSpellCasting() {
		return spellCasting;
	}
	
	public void update() {
		if (curHealth > 0) {
			move();
			gravity();
			updateSprite();
		} else {
			alive = false;
		}
	}
	
	public void applyRegenEffects() {
		if (alive) {
			// Health Regeneration
			if (curHealth + healthRegeneration > maxHealth) {
				curHealth = maxHealth;
			} else {
				curHealth += healthRegeneration;
			}
			
			// Resource Regeneration
			if (resource + resourceRegeneration > maxResource) {
				resource = maxResource;
			} else {
				resource += resourceRegeneration;
			}
		}
	}
	
	public void learnSpell(Spell spell) {
		Spell spellCopy = new Spell(spell, this);
		spellBook.put(spellCopy.getName(), spellCopy);
	}
	
	public boolean verifySpellCast(Spell spell) {
		if (facing == DirectionFacing.forward || facing == DirectionFacing.back)
			return false;
		
		if (moving)
			return false;
		
		if (spell.getCostType() != resourceType)
			return false;
		
		if (spell.getCost() > resource)
			return false;
		
		return true;
	}
	
	protected void applySpellCost(int cost) {
		setResourceValue(resource - cost);
	}
	
	public void cancelSpellCast() {
		// allow finish shooting
		if (!shooting) {
			if (castTimer.isRunning())
				castTimer.stop();
			casting = false;
			spellCasting = null;
		}
	}
	
	public void castSpell(Spell spell) {
		if (casting) {
			if (castTimer.isRunning()) {
				castTimer.restart();
			}
		} else {
			if (verifySpellCast(spell)) {
				casting = true;
				spellCasting = spell;
				castTimer = new Timer(spell.getCastTime(), new ActionListener() {
	
					@Override
					public void actionPerformed(ActionEvent e) {
						if (!shooting) {
							applySpellCost(spell.getCost());
							if (spell.getDummy().getMoves()) {
								SpellDummy dummy = spell.getNewDummy();
								dummy.display(self);
								spellDummies.add(dummy);
							} else
								spell.getEffect().applyEffect(self, self);
							shooting = true;
							if (castTimer.isRunning())
								castTimer.stop();
							castTimer.setInitialDelay(200);
							castTimer.start();
						} else {
							shooting = false;
							if (castTimer.isRunning())
								castTimer.stop();
							casting = false;
							spellCasting = null;
						}
					}
					
				});
				castTimer.setInitialDelay(spell.getCastTime());
				castTimer.setRepeats(false);
				castTimer.start();
			} else {
				System.out.println("Failed to cast spell");
			}
		}
	}

	public void takeSpellDamage(SpellEffect spellEffect, Unit fromUnit) {
		int val = spellEffect.getValue();
		float newVal = val;
		
		// Account for level difference
		float levelDiff = fromUnit.getLevel() - getLevel();
		newVal += (newVal*(levelDiff/10));
		
		// 25% more damage
		float weaknessMod = 0.25f;
		float strengthMod = 0.25f;
		switch (spellEffect.getDamageType()) {
		case damage_physical:
			if (weaknesses.contains(SpellDamageType.damage_physical))
				newVal -= (val*weaknessMod);
			if (strengths.contains(SpellDamageType.damage_physical))
				newVal += (val*strengthMod);
			newVal -= armor;
			break;
		case damage_arcane:
			if (weaknesses.contains(SpellDamageType.damage_arcane))
				newVal -= (val*weaknessMod);
			if (strengths.contains(SpellDamageType.damage_arcane))
				newVal += (val*strengthMod);
			break;
		case damage_fire:
			if (weaknesses.contains(SpellDamageType.damage_fire))
				newVal -= (val*weaknessMod);
			if (strengths.contains(SpellDamageType.damage_fire))
				newVal += (val*strengthMod);
			break;
		case damage_frost:
			if (weaknesses.contains(SpellDamageType.damage_frost))
				newVal -= (val*weaknessMod);
			if (strengths.contains(SpellDamageType.damage_frost))
				newVal += (val*strengthMod);
			break;
		case damage_light:
			if (weaknesses.contains(SpellDamageType.damage_light))
				newVal -= (val*weaknessMod);
			if (strengths.contains(SpellDamageType.damage_light))
				newVal += (val*strengthMod);
			break;
		case damage_shadow:
			if (weaknesses.contains(SpellDamageType.damage_shadow))
				newVal -= (val*weaknessMod);
			if (strengths.contains(SpellDamageType.damage_shadow))
				newVal += (val*strengthMod);
			break;
		default:
			break;
		}
		
		takeDamage((int)newVal);
		
		if (this != fromUnit) {
			try {
				// only add threat if creature
				toCreature().addThreat(fromUnit, newVal);
				if (curHealth <= 0) {
					// fromUnit got killing blow on this unit
					toCreature().die(fromUnit.toPlayer());
				}
			} catch(Exception e) {
				// only throws if spell hit player
				//e.printStackTrace();
			}
		}
	}
	
	public void dropItem(Item item) {
		//TODO implement
	}
	
	public void takeDamage(int val) {
		setCurHealth(curHealth - val);
		if (val > 0)
			changes.add(new UnitStatusChange(this, String.valueOf(-val), StatusChangeType.damage));
		else if (val == 0)
			changes.add(new UnitStatusChange(this, String.valueOf(-val), StatusChangeType.none));
		else if (val < 0)
			changes.add(new UnitStatusChange(this, "+" + String.valueOf(-val), StatusChangeType.heal));
	}
	
	public void displayStatusChanges(Graphics g) {
		for (int i = 0; i < changes.size(); i++) {
			UnitStatusChange change = changes.get(i);
			change.displayStatusChange(g);
			if (change.getCurrentY() < change.getStartingY() - 100) {
				changes.remove(i);
				i--;
			}
		}
	}
	
	public ArrayList<UnitStatusChange> getStatusChanges() {
		return changes;
	}
	
	public Creature toCreature() {
		return (Creature)this;
	}
	
	public void moveInDirection(DirectionFacing direction) {
		switch (direction) {
		case forward:
			setDX(0);
			break;
		case left:
			setDirectionFacing(direction);
			setDX(-UNIT_SPEED);
			break;
		case right:
			setDirectionFacing(direction);
			setDX(UNIT_SPEED);
			break;
		default:
			break;
		}
	}
	
	public void move() {
		if (dx != 0 || dy != 0) {
			moving = true;
		} else
			moving = false;
		
		x += dx;
		y += dy;
		
		if (x < 0)
			x = 0;
		
		if (x > MyRPGFrame.wWidth - width)
			x = MyRPGFrame.wWidth - width;
		
		if (y > MyRPGFrame.wHeight - height)
			y = MyRPGFrame.wHeight - height;
		
		if (y < -5)
			y = -5;
	}
	
	public void gravity() {
		if (gravityActive)
			dy += GRAVITY_WEIGHT;
		else 
			dy = 0;
	}
	
	public void jump() {
		if (!jumping && !gravityActive) {
			jumping = true;
			dy += JUMPING_SPEED;
			gravityActive = true;
		}
	}
	
	public void setDX(int val) {
		dx = val;
	}
	
	public void setDY(int val) {
		dy = val;
	}
	
	public boolean isWalking() {
		return walking;
	}
	
	public boolean isCasting() {
		return casting;
	}
	
	public boolean isShooting() {
		return shooting;
	}
	
	public boolean isMoving() {
		return moving;
	}
	
	public Player toPlayer() {
		return (Player)this;
	}
	
	public SpellDummy toSpellDummy() {
		return (SpellDummy)this;
	}
	
	public void setMovementSpeed(int speed) {
		UNIT_SPEED = speed;
	}
	
	public int getMovementSpeed() {
		return UNIT_SPEED;
	}
	
	public void printAllStats() {
		HashMap<StatType, Integer> allStats = getAllStats();
		System.out.println("Printing " + name + "'s Stats");
		for (StatType type: StatType.values()) {
			System.out.println("    " + type + ": " + allStats.get(type));
		}
		System.out.println("End of Stats");
	}
}
