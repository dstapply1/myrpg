package myrpg.entities;

import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.Timer;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import myrpg.game.Game;
import myrpg.items.Inventory;
import myrpg.items.InventorySlot;
import myrpg.items.Item;
import myrpg.items.ItemInventoryType;
import myrpg.items.Stat;
import myrpg.items.StatType;
import myrpg.spells.Spell;
import myrpg.spells.SpellDummy;
import myrpg.view.MyRPGFrame;

public class Player extends Unit {
	private int curExp;
	private int expToNextLevel;
	private int timeCasting;
	private Inventory inventory;
	private int gold;
	private boolean inInventory;
	
	private Timer castTimeUpdater;
	
	// key action stuff
	private ArrayList<Integer> keysPressed;
	
	public Player(String spriteDirPath, String name) {
		super(spriteDirPath);
		this.name = name;
		x = 500;
		y = 300;
		curExp = 0;
		timeCasting = 0;
		expToNextLevel = (int) Math.sqrt(100*level);
		inventory = new Inventory(this);
		castTimeUpdater = new Timer(50, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (casting)
					timeCasting += 50;
				else
					timeCasting = 0;
			}
		});
		castTimeUpdater.start();
		gold = 0;
		inInventory = false;
		keysPressed = new ArrayList<Integer>();
	}
	
	public int getCurExp() {
		return curExp;
	}
	
	public int getExpToNextLevel() {
		return expToNextLevel;
	}
	
	public void gainExp(int val) {
		if (val + curExp >= expToNextLevel) {
			levelUp();
			curExp = (val + curExp) - expToNextLevel;
		} else {
			curExp += val;
		}
		changes.add(new UnitStatusChange(this, "+" + String.valueOf(val), StatusChangeType.exp));
	}
	
	public void levelUp() {
		// play level up animation
		
		changes.add(new UnitStatusChange(this, "LEVEL UP!!", StatusChangeType.levelup));
		level++;
		for (StatType type: StatType.values()) {
			baseStats.put(type, baseStats.get(type) + 1);
		}
		expToNextLevel = (int) Math.sqrt(100*level);
		// TODO check learn new abilities etc.
		
	}
	
	public Inventory getInventory() {
		return inventory;
	}
	
	public boolean isInInventory() {
		return inInventory;
	}
	
	public void setInInventory(boolean val) {
		inInventory = val;
	}
	
	public boolean lootItem(Item item) {
		return inventory.addItem(item);
	}
	
	public void printInventory() {
		System.out.println("Printing " + name + "'s Inventory: ");
		for (InventorySlot slot: inventory.getSlots()) {
			if (!slot.isEmpty())
				System.out.println("    " + slot.getSlotType() + ": " + slot.getItem().getName());
			else
				System.out.println("    " + slot.getSlotType() + ": ");
		}
		System.out.println("End of Inventory");
	}
	
	public int getTimeCasting() {
		return timeCasting;
	}
	
	@Override
	public void cancelSpellCast() {
		// allow finish shooting
		if (!shooting) {
			if (castTimer.isRunning())
				castTimer.stop();
			casting = false;
			spellCasting = null;
		}
	}
	
	@Override
	public void castSpell(Spell spell) {
		if (casting) {
			if (castTimer.isRunning()) {
				castTimer.restart();
				timeCasting = 0;
			}
		} else {
			if (verifySpellCast(spell)) {
				casting = true;
				spellCasting = spell;
				castTimer = new Timer(spell.getCastTime(), new ActionListener() {
	
					@Override
					public void actionPerformed(ActionEvent e) {
						if (!shooting) {
							applySpellCost(spell.getCost());
							if (spell.getDummy().getMoves()) {
								SpellDummy dummy = spell.getNewDummy();
								dummy.display(self);
								spellDummies.add(dummy);
							} else
								spell.getEffect().applyEffect(self, self);
							shooting = true;
							if (castTimer.isRunning())
								castTimer.stop();
							castTimer.setInitialDelay(200);
							castTimer.start();
						} else {
							shooting = false;
							if (castTimer.isRunning())
								castTimer.stop();
							casting = false;
							spellCasting = null;
						}
					}
					
				});
				castTimer.setInitialDelay(spell.getCastTime());
				castTimer.setRepeats(false);
				castTimer.start();
			} else {
				System.out.println("Failed to cast spell");
			}
		}
	}
	
	@Override
	public void move() {
		if (dx != 0 || dy != 0) {
			moving = true;
			if (casting)
				cancelSpellCast();
		} else {
			moving = false;
			return;
		}
		
		x += dx;
		y += dy;
		
		if (x < -1*(width/2)) {
			if (Game.getCurrentMap().hasPreviousArea(area)) {
				area--;
				Game.changeArea(this);
				x = MyRPGFrame.wWidth - width/2;
			} else
				x = -1*(width/2);
		}
		
		if (x > MyRPGFrame.wWidth - width/2) {
			if (Game.getCurrentMap().hasNextArea(area)) {
				area++;
				Game.changeArea(this);
				x = -1*(width/2);
			} else
				x = MyRPGFrame.wWidth - width/2;
		}
		
		if (y > MyRPGFrame.wHeight - height)
			y = MyRPGFrame.wHeight - height;
		
		if (y < -5)
			y = -5;
	}
	
	private void walkInDirection(DirectionFacing direction) {
		moveInDirection(direction);
		walking = true;
	}
	
	private void stopWalking() {
		dx = 0;
		walking = false;
	}

	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		
		if (!keysPressed.contains((Object)key)) {
			keysPressed.add(key);
		}
		
		//Character actions
		if (key == KeyEvent.VK_UP) {
			jump();
		}
		if (key == KeyEvent.VK_LEFT) {
			walkInDirection(DirectionFacing.left);
		}
		if (key == KeyEvent.VK_RIGHT) {
			walkInDirection(DirectionFacing.right);
		}
		if (key == KeyEvent.VK_SPACE) {
			castSpell(spellBook.get("Fireball"));
		}
		if (key == KeyEvent.VK_1) {
			takeDamage(10);
		}
		if (key == KeyEvent.VK_2) {
			takeDamage(-10);
		}
		if (key == KeyEvent.VK_3) {
			gainExp(5);
		}
		if (key == KeyEvent.VK_0) {
			printAllStats();
		}
		if (key == KeyEvent.VK_5) {
			ArrayList<Stat> tStats = new ArrayList<Stat>();
			tStats.add(new Stat(StatType.agility, 3));
			tStats.add(new Stat(StatType.stamina, 4));
			Item thunderfury = new Item("Thunderfury", ItemInventoryType.oneHand, tStats);
			lootItem(thunderfury);
		}
		if (key == KeyEvent.VK_I) {
			inInventory = true;
			printInventory();
		}
		// Unused
		/*
		if (key == KeyEvent.VK_DOWN) {
			dy = CHAR_SPEED;
		}
		*/
	}
	
	public void keyReleased(KeyEvent e) {
		int key = e.getKeyCode();
		
		if (keysPressed.contains((Object)key))
			keysPressed.remove((Integer)key);
		
		if (key == KeyEvent.VK_LEFT) {
			if (!keysPressed.contains(KeyEvent.VK_RIGHT)) {
				stopWalking();
			} else {
				walkInDirection(DirectionFacing.right);
			}
		}
		if (key == KeyEvent.VK_RIGHT) {
			if (!keysPressed.contains(KeyEvent.VK_LEFT)) {
				stopWalking();
			} else {
				walkInDirection(DirectionFacing.left);
			}
		}
		
		// Unused Keys
		/*
		// Only Jump on key up press
		if (key == KeyEvent.VK_UP) {
			
		}
		
		if (key == KeyEvent.VK_DOWN) {
			dy = 0;
		}
		*/
	}
}
