package myrpg.entities;

import myrpg.spells.Spell;
import myrpg.sprites.Sprite;
import myrpg.sprites.SpriteType;

public class Game_Object extends Entity {
	
	private boolean collisionOn;
	private boolean avoidSpellCollision;
	private Spell spell;
	
	public Game_Object(String spriteDirPath, int x, int y, boolean collisionOn, boolean visible) {
		super();
		sprite = new Sprite(spriteDirPath, null, SpriteType.gameobject);
		this.x = x;
		this.y = y;
		this.collisionOn = collisionOn;
		this.visible = visible;
		avoidSpellCollision = false;
		updateDimensions();
	}
	
	public void setCollisionOn(boolean val) {
		collisionOn = val;
	}
	
	public boolean isCollisionOn() {
		return collisionOn;
	}
	
	public void setAvoidSpellCollision(boolean val) {
		avoidSpellCollision = val;
	}
	
	public boolean isAvoidSpellCollisionOn() {
		return avoidSpellCollision;
	}
	
	public Spell getSpell() {
		return spell;
	}
	
	public void setSpell(Spell spell) {
		this.spell = spell;
	}
}
