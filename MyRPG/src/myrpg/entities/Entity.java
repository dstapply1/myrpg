package myrpg.entities;

import java.awt.Image;
import java.awt.Rectangle;

import myrpg.locations.GameMapId;
import myrpg.sprites.Sprite;

public abstract class Entity {
	protected float dx, dy;
	protected int x, y;
	protected GameMapId mapId;
	protected int area;
	protected int width, height;
	protected boolean visible;
	protected Sprite sprite;
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setX(int val) {
		x = val;
	}
	
	public void setY(int val) {
		y = val;
	}
	
	public void setDX(int val) {
		dx = val;
	}
	
	public void setDY(int val) {
		dy = val;
	}
	
	public float getDX() {
		return dx;
	}
	
	public float getDY() {
		return dy;
	}
	
	public boolean isVisible() {
		return visible;
	}
	
	public void setVisible(boolean val) {
		visible = val;
	}
	
	public Rectangle getBounds() {
		return new Rectangle(x, y, width, height);
	}
	
	public Image getCurrentImage() {
		return sprite.getCurrentImage();
	}
	
	public void updateDimensions() {
		if (sprite.getCurrentImage() != null) {
			width = sprite.getCurrentImage().getWidth(null);
			height = sprite.getCurrentImage().getHeight(null);
		}
	}
	
	public void updateSprite() {
		sprite.updateCurrentImage();
	}

	public GameMapId getMapId() {
		return mapId;
	}

	public void setMapId(GameMapId mapId) {
		this.mapId = mapId;
	}

	public int getArea() {
		return area;
	}

	public void setArea(int area) {
		this.area = area;
	}
	
	
}
