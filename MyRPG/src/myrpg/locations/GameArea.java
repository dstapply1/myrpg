package myrpg.locations;

import java.util.ArrayList;

import myrpg.entities.Game_Object;
import myrpg.entities.Unit;

public class GameArea {
	private int id;
	private String name;
	private ArrayList<Unit> units;
	private ArrayList<Game_Object> gameObjects;
	
	public GameArea(int id, String name) {
		this.id = id;
		this.name = name;
		units = new ArrayList<Unit>();
		gameObjects = new ArrayList<Game_Object>();
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public ArrayList<Unit> getUnits() {
		return units;
	}
	
	public ArrayList<Game_Object> getGameObjects() {
		return gameObjects;
	}
	
	public void addUnit(Unit unit) {
		units.add(unit);
	}
	
	public void addGameObject(Game_Object go) {
		go.setArea(id);
		gameObjects.add(go);
	}
}
