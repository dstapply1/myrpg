package myrpg.locations;

import java.util.ArrayList;

public class GameMap {
	private GameMapId id;
	private ArrayList<GameArea> areas;
	private String name;
	
	public GameMap(String name) {
		this.name = name;
		areas = new ArrayList<GameArea>();
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public ArrayList<GameArea> getAreas() {
		return areas;
	}
	
	public void addArea(GameArea area) {
		areas.add(area);
	}
	
	public boolean hasNextArea(int currentArea) {
		if (areas.size() > currentArea + 1)
			return true;
		return false;
	}
	
	public boolean hasPreviousArea(int currentArea) {
		if (currentArea - 1 >= 0)
			return true;
		return false;
	}
}
