package myrpg.view;

import java.awt.event.KeyEvent;
import java.util.ArrayList;

import myrpg.game.Game;

public class Menu {
	
	private Game game;
	private ArrayList<MenuItem> menu;
	private boolean inMenu;
	private MenuItem exit, restart, backtogame, stuff, startgame, newgame;
	
	public Menu(Game game) {
		menu = new ArrayList<MenuItem>();
		inMenu = false;
		this.game = game;
	}
	
	public ArrayList<MenuItem> getMenu() {
		return menu;
	}
	
	public boolean isInMenu() {
		return inMenu;
	}
	
	public void setInMenu(boolean val) {
		inMenu = val;
	}
	
	public void selectNextDownMenuItem() {
		for (int i = 0; i < menu.size(); i++) {
			if (menu.get(i).isSelected()) {
				if (i != 0) {
					menu.get(i).setSelected(false);
					menu.get(i-1).setSelected(true);
				} else {
					menu.get(i).setSelected(false);
					menu.get(menu.size()-1).setSelected(true);
				}
				break;
			}
		}
	}
	
	public void selectNextUpMenuItem() {
		for (int i = 0; i < menu.size(); i++) {
			if (menu.get(i).isSelected()) {
				if (i != menu.size()-1) {
					menu.get(i).setSelected(false);
					menu.get(i+1).setSelected(true);
				} else {
					menu.get(i).setSelected(false);
					menu.get(0).setSelected(true);
				}
				break;
			}
		}
	}
	
	public void resetSelectedMenuItem() {
		for (int i = 0; i < menu.size(); i++) {
			if (menu.get(i).isSelected()) {
				menu.get(i).setSelected(false);
			}
		}
		menu.get(menu.size()-1).setSelected(true);
	}
	
	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		
		if (key == KeyEvent.VK_DOWN) {
			selectNextDownMenuItem();
		}
		
		if (key == KeyEvent.VK_UP) {
			selectNextUpMenuItem();
		}
		
		if (key == KeyEvent.VK_ENTER) {
			for (int i = 0; i < menu.size(); i++) {
				if (menu.get(i).isSelected()) {
					if (menu.get(i).equals(exit)) {
						System.exit(0);
					} else if (menu.get(i).equals(backtogame)) {
						setInMenu(false);
						game.setInGame(true);
					} else if (menu.get(i).equals(newgame)) {
						game.RestartGame();
					} else if (menu.get(i).equals(restart)) {
						game.RestartGame();
					} else if (menu.get(i).equals(startgame)) {
						setInMenu(false);
						game.setInGame(true);
					} else if (menu.get(i).equals(stuff)) {
						// Settings?
					}
				}
			}
		}
		
		if (key == KeyEvent.VK_ESCAPE) {
			if (inMenu) {
				resetSelectedMenuItem();
			}
		}
	}
	
	// Premake Menus
	public void initStartMenu() {
		exit = new MenuItem("Exit", false);
		stuff = new MenuItem("Stuff", false);
		startgame = new MenuItem("Start Game", true);
		
		menu.add(exit);
		menu.add(stuff);
		menu.add(startgame);
		
		inMenu = true;
	}
	
	public void initInGameMenu() {
		exit = new MenuItem("Exit", false);
		restart = new MenuItem("Restart", false);
		backtogame = new MenuItem("Back to Game", true);
		
		menu.add(exit);
		menu.add(restart);
		menu.add(backtogame);
	}
	
	public void initGOMenu() {
		exit = new MenuItem("Exit", false);
		newgame = new MenuItem("New Game", true);
		menu.add(exit);
		menu.add(newgame);
	}
}
