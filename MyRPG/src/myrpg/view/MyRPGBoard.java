package myrpg.view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JPanel;
import javax.swing.Timer;

import myrpg.game.Game;

@SuppressWarnings("serial")
public class MyRPGBoard extends JPanel implements ActionListener {
	
	private Game game;
	
	//private Timer drawingTimer;
	private Timer gameTimer;
	
	public MyRPGBoard() {
		initBoard();
		startGame();
	}
	
	private void initBoard() {
		addKeyListener(new TAdapter());
		setFocusable(true);
		setBackground(Color.GRAY);
		setDoubleBuffered(true);
	}
	
	private void startGame() {
		game = new Game(this);
		
		//drawingTimer = new Timer(16, this);
		//drawingTimer.start();
		
		gameTimer = new Timer(16, this);
		gameTimer.start();
	}
	
	public void paint(Graphics g) {
		super.paint(g);
		
		game.draw(g);
	}
	
	private class TAdapter extends KeyAdapter {
		
		public void keyReleased(KeyEvent e) {
			game.keyReleased(e);
		}
		
		public void keyPressed(KeyEvent e) {
			int key = e.getKeyCode();
			
			game.keyPressed(e);
		}
	}

	public void actionPerformed(ActionEvent e) {
		game.actionPerformed(e);
		repaint();
	}
}
