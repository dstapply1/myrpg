package myrpg.view;

import javax.swing.JFrame;

@SuppressWarnings("serial")
public class MyRPGFrame extends JFrame {
	public static int wWidth = 1280;
	public static int wHeight = 720;
	
	public MyRPGFrame() {
		add(new MyRPGBoard());
		setTitle("MyRPG");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(wWidth, wHeight);
		setLocationRelativeTo(null);
		setVisible(true);
		setResizable(false);
	}
	
	public static void main(String[] args) {
		System.setProperty("sun.java2d.opengl", "true");
		new MyRPGFrame();
	}
} 
