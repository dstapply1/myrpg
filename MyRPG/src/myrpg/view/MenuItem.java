package myrpg.view;

public class MenuItem {
	
	private String text;
	private boolean selected;
	
	public MenuItem(String text, boolean selected) {
		this.text = text;
		this.selected = selected;
	}
	
	public String getText() {
		return text;
	}
	
	public boolean isSelected() {
		return selected;
	}
	
	public void setSelected(boolean val) {
		selected = val;
	}
}
