package myrpg.sprites;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.Timer;

import myrpg.entities.DirectionFacing;
import myrpg.entities.Unit;

public class Sprite implements ActionListener {
	/** Unit that this sprite is for */
	private Unit owner;
	/** Store images */
	private Image frontImage, backImage;
	private Image rightStandingImage;
	private Image leftStandingImage;
	private ArrayList<Image> rightWalkingImages, rightJumpingImages, rightCastingImages, rightShootingImages;
	private ArrayList<Image> leftWalkingImages, leftJumpingImages, leftCastingImages, leftShootingImages;
	private ArrayList<Image> spellEffectImages;
	private ArrayList<Image> currentAnimation;
	private boolean currentlyAnimating;
	
	private Timer timer;
	
	/** Image that is drawn */
	private Image currentImage;
	
	/** Used to keep track of current animation */
	private int nextImageIndex;
	
	private SpriteState lastState;
	private SpriteState currentState;
	
	private SpriteType type;
	
	/* Primary constructor:
	 * Pass a sprite directory that
	 * contains all of the images to
	 * fill the sprite.
	 */
	public Sprite(String dirPath, Unit owner, SpriteType type) {
		this.owner = owner;
		this.type = type;
		timer = new Timer(100, this);
		currentlyAnimating = false;
		nextImageIndex = 0;
		rightWalkingImages = new ArrayList<Image>();
		rightJumpingImages = new ArrayList<Image>();
		rightCastingImages = new ArrayList<Image>();
		rightShootingImages = new ArrayList<Image>();
		leftWalkingImages = new ArrayList<Image>();
		leftJumpingImages = new ArrayList<Image>();
		leftCastingImages = new ArrayList<Image>();
		leftShootingImages = new ArrayList<Image>();
		spellEffectImages = new ArrayList<Image>();
		
		ArrayList<String> images = getImagesFromDir(dirPath);
		
		for (int i = 0; i < images.size(); i++) {
			ImageIcon icon = new ImageIcon(images.get(i));
			
			//figure out which image it is
			if (images.get(i).contains("_front")) {
				System.out.println("Setting primary image");
				frontImage = icon.getImage();
			} else if (images.get(i).contains("_back")) {
				backImage = icon.getImage();
			} else if (images.get(i).contains("_leftStanding")) {
				leftStandingImage = icon.getImage();
			} else if (images.get(i).contains("_rightStanding")) {
				rightStandingImage = icon.getImage();
			} else if (images.get(i).contains("_leftWalking")) {
				leftWalkingImages.add(icon.getImage());
			} else if (images.get(i).contains("_rightWalking")) {
				rightWalkingImages.add(icon.getImage());
			} else if (images.get(i).contains("_leftJumping")) {
				leftJumpingImages.add(icon.getImage());
			} else if (images.get(i).contains("_rightJumping")) {
				rightJumpingImages.add(icon.getImage());
			} else if (images.get(i).contains("_leftCasting")) {
				leftCastingImages.add(icon.getImage());
			} else if (images.get(i).contains("_rightCasting")) {
				rightCastingImages.add(icon.getImage());
			} else if (images.get(i).contains("_leftShoot")) {
				leftShootingImages.add(icon.getImage());
			} else if (images.get(i).contains("_rightShoot")) {
				rightShootingImages.add(icon.getImage());
			} else if (images.get(i).contains("spell") && images.get(i).contains("collision")) {
				spellEffectImages.add(icon.getImage());
			} else {
				System.err.println("Sprite loaded an unknown image..");
			}
		}
		
		// Initialize to primary, all sprites need one..
		if (type == SpriteType.spell && owner != null) {
			// spells initialize image to left or right
			// initialize state to front to force update into animation
			if (owner.getDirectionFacing() == DirectionFacing.left) {
				currentImage = getNextImage(leftWalkingImages);
			} else if (owner.getDirectionFacing() == DirectionFacing.right) {
				currentImage = getNextImage(rightWalkingImages);
			}
			currentState = SpriteState.front;
			lastState = SpriteState.front;
		} else {
			if (frontImage != null) {
				currentImage = frontImage;
				currentState = SpriteState.front;
				lastState = SpriteState.front;
			} else {
				System.err.println("Sprite is missing frontImage....");
				
				currentState = SpriteState.error;
				currentState = SpriteState.error;
			}
		}
	}
	
	/*
	 * Used for sprites with one image
	 */
	public Sprite(Image frontImage, SpriteType type) {
		this.type = type;
		this.frontImage = frontImage;
		currentImage = frontImage;
		currentState = SpriteState.front;
		lastState = currentState;
	}
	
	public Sprite(Sprite original, Unit owner, SpriteType type) {
		this.type = type;
		this.owner = owner;
		timer = new Timer(100, this);
		currentlyAnimating = false;
		nextImageIndex = 0;
		
		frontImage = original.frontImage;
		backImage = original.backImage;
		rightStandingImage = original.rightStandingImage;
		leftStandingImage = original.leftStandingImage;
		rightWalkingImages = original.rightWalkingImages;
		leftWalkingImages = original.leftWalkingImages;
		rightJumpingImages = original.rightJumpingImages;
		leftJumpingImages = original.leftJumpingImages;
		rightCastingImages = original.rightCastingImages;
		leftCastingImages = original.leftCastingImages;
		rightShootingImages = original.rightShootingImages;
		leftShootingImages = original.leftShootingImages;
		spellEffectImages = original.spellEffectImages;
		
		// Initialize to primary, all sprites need one..
		if (type == SpriteType.spell && owner != null) {
			// spells initialize image to left or right
			// initialize state to front to force update into animation
			if (owner.getDirectionFacing() == DirectionFacing.left) {
				currentImage = getNextImage(leftWalkingImages);
			} else if (owner.getDirectionFacing() == DirectionFacing.right) {
				currentImage = getNextImage(rightWalkingImages);
			}
			currentState = SpriteState.front;
			lastState = SpriteState.front;
		} else {
			if (frontImage != null) {
				currentImage = frontImage;
				currentState = SpriteState.front;
				lastState = SpriteState.front;
			} else {
				System.err.println("Sprite is missing frontImage....");
				
				currentState = SpriteState.error;
				currentState = SpriteState.error;
			}
		}
	}
	
	public Image getCurrentImage() {
		return currentImage;
	}
	
	public void setOwner(Unit owner) {
		this.owner = owner;
	}
	
	private ArrayList<String> getImagesFromDir(String dirPath) {
		ArrayList<String> images = new ArrayList<String>();
		try {
			Files.walk(Paths.get(dirPath)).forEach(filePath -> {
			    if (Files.isRegularFile(filePath)) {
			        if (filePath.toString().endsWith(".jpg") || filePath.toString().endsWith(".png")) {
			        	System.out.println(filePath);
			        	images.add(filePath.toString());
			        }
			    }
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
		return images;
	}
	
	public void updateCurrentImage() {
		if (owner != null) {
			if (type == SpriteType.spell) {
				if (owner.toSpellDummy().isSpellEffectActive()) {
					setState(SpriteState.spellEffect);
				} else if (owner.getDirectionFacing() == DirectionFacing.left) {
					if (owner.isWalking()) {
						setState(SpriteState.leftWalking);
					}
				} else if (owner.getDirectionFacing() == DirectionFacing.right) {
					if (owner.isWalking()) {
						setState(SpriteState.rightWalking);
					}
				}
			} else {
				//Check unit state change
				if (owner.getDirectionFacing() == DirectionFacing.forward) {
					setState(SpriteState.front);
				} else if (owner.getDirectionFacing() == DirectionFacing.back) {
					setState(SpriteState.back);
				} else if (owner.getDirectionFacing() == DirectionFacing.left) {
					if (owner.isWalking()) {
						if (owner.isJumping()) {
							if (owner.isCasting()) {
								setState(SpriteState.leftWalkJumpCasting);
							} else if (owner.isShooting())
								setState(SpriteState.leftWalkJumpShooting);
							else
								setState(SpriteState.leftWalkJumping);
						} else if (owner.isCasting()) {
							setState(SpriteState.leftWalkCasting);
						} else if (owner.isShooting()) {
							setState(SpriteState.leftWalkShooting);
						} else {
							setState(SpriteState.leftWalking);
						}
					} else if (owner.isJumping()) {
						setState(SpriteState.leftJumping);
					} else if (owner.isCasting()) {
						if (owner.isShooting())
							setState(SpriteState.leftShooting);
						else
							setState(SpriteState.leftCasting);
					} else if (owner.isShooting()) {
						setState(SpriteState.leftShooting);
					} else {
						setState(SpriteState.leftStanding);
					}
				} else if (owner.getDirectionFacing() == DirectionFacing.right) {
					if (owner.isWalking()) {
						if (owner.isJumping()) {
							if (owner.isCasting()) {
								setState(SpriteState.rightWalkJumpCasting);
							} else if (owner.isShooting())
								setState(SpriteState.rightWalkJumpShooting);
							else
								setState(SpriteState.rightWalkJumping);
						} else if (owner.isCasting()) {
							setState(SpriteState.rightWalkCasting);
						} else if (owner.isShooting()) {
							setState(SpriteState.rightWalkShooting);
						} else {
							setState(SpriteState.rightWalking);
						}
					} else if (owner.isJumping()) {
						setState(SpriteState.rightJumping);
					} else if (owner.isCasting()) {
						if (owner.isShooting())
							setState(SpriteState.rightShooting);
						else
							setState(SpriteState.rightCasting);
					} else if (owner.isShooting()) {
						setState(SpriteState.rightShooting);
					} else {
						setState(SpriteState.rightStanding);
					}
				}
			}
			
			if (hasStateChanged()) {
				//System.out.println("Current Sprite State: " + currentState.toString());
				switch(currentState) {
				case error:
					System.err.println("Sprite in Error State");
					break;
				case front:
					stopCurrentAnimation();
					currentImage = frontImage;
					setState(SpriteState.front);
					break;
				case back:
					stopCurrentAnimation();
					currentImage = backImage;
					setState(SpriteState.back);
					break;
				case leftStanding:
					stopCurrentAnimation();
					currentImage = leftStandingImage;
					setState(SpriteState.leftStanding);
					break;
				case rightStanding:
					stopCurrentAnimation();
					currentImage = rightStandingImage;
					setState(SpriteState.rightStanding);
					break;
				case leftWalking:
					stopCurrentAnimation();
					playAnimation(leftWalkingImages);
					setState(SpriteState.leftWalking);
					break;
				case rightWalking:
					stopCurrentAnimation();
					playAnimation(rightWalkingImages);
					setState(SpriteState.rightWalking);
					break;
				case leftJumping:
					stopCurrentAnimation();
					playAnimation(leftJumpingImages);
					setState(SpriteState.leftJumping);
					break;
				case rightJumping:
					stopCurrentAnimation();
					playAnimation(rightJumpingImages);
					setState(SpriteState.rightJumping);
					break;
				case leftCasting:
					stopCurrentAnimation();
					playAnimation(leftCastingImages);
					setState(SpriteState.leftCasting);
					break;
				case rightCasting:
					stopCurrentAnimation();
					playAnimation(rightCastingImages);
					setState(SpriteState.rightCasting);
					break;
				case leftShooting:
					stopCurrentAnimation();
					playAnimation(leftShootingImages);
					setState(SpriteState.leftShooting);
					break;
				case rightShooting:
					stopCurrentAnimation();
					playAnimation(rightShootingImages);
					setState(SpriteState.rightShooting);
					break;
				case leftWalkJumping:
					stopCurrentAnimation();
					playAnimation(leftJumpingImages);
					setState(SpriteState.leftWalkJumping);
					break;
				case rightWalkJumping:
					stopCurrentAnimation();
					playAnimation(rightJumpingImages);
					setState(SpriteState.rightWalkJumping);
					break;
				case leftWalkJumpCasting:
					stopCurrentAnimation();
					playAnimation(leftCastingImages);
					setState(SpriteState.leftWalkJumpCasting);
					break;
				case rightWalkJumpCasting:
					stopCurrentAnimation();
					playAnimation(rightCastingImages);
					setState(SpriteState.rightWalkJumpCasting);
					break;
				case leftWalkJumpShooting:
					stopCurrentAnimation();
					playAnimation(leftShootingImages);
					setState(SpriteState.leftWalkJumpShooting);
					break;
				case rightWalkJumpShooting:
					stopCurrentAnimation();
					playAnimation(rightShootingImages);
					setState(SpriteState.rightWalkJumpShooting);
					break;
				case spellEffect:
					stopCurrentAnimation();
					playAnimation(spellEffectImages);
					setState(SpriteState.spellEffect);
					break;
				}
				owner.updateDimensions();
			}
		} else {
			System.err.println("Sprite doesnt have owner");
		}
	}
	
	public boolean hasStateChanged() {
		if (currentState != lastState)
			return true;
		
		return false;
	}
	
	public void setState(SpriteState state) {
		lastState = currentState;
		currentState = state;
	}
	
	private Image getNextImage(ArrayList<Image> images) {
		if (images.size() > nextImageIndex) {
			return images.get(nextImageIndex++);
		} else {
			if (currentState == SpriteState.spellEffect) {
				owner.setVisible(false);
				owner.toSpellDummy().setEffectFinished(true);
			}
			// reset animation
			nextImageIndex = 0;
			return images.get(nextImageIndex);
		}
	}
	
	private void stopCurrentAnimation() {
		if (timer.isRunning()) {
			timer.stop();
			currentlyAnimating = false;
		}
	}
	
	private void playAnimation(ArrayList<Image> images) {
		if (!currentlyAnimating) {
			currentAnimation = images;
			timer.setInitialDelay(0);
			timer.start();
			currentlyAnimating = true;
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (currentlyAnimating) {
			currentImage = getNextImage(currentAnimation);
		}
	}
	
}
