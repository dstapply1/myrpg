package myrpg.sprites;

public enum SpriteState {
	error,
	front,
	back,
	leftStanding,
	rightStanding,
	leftWalking,
	rightWalking,
	leftJumping,
	rightJumping,
	leftCasting,
	rightCasting,
	leftShooting,
	rightShooting,
	leftWalkJumping,
	rightWalkJumping,
	leftWalkCasting,
	rightWalkCasting,
	leftWalkShooting,
	rightWalkShooting,
	leftWalkJumpCasting,
	rightWalkJumpCasting,
	leftWalkJumpShooting,
	rightWalkJumpShooting,
	spellEffect
}
