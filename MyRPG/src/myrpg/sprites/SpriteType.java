package myrpg.sprites;

public enum SpriteType {
	unit,
	player,
	npc,
	gameobject,
	spell,
	UI
}
