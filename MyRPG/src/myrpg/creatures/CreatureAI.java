package myrpg.creatures;

import myrpg.entities.DirectionFacing;
import myrpg.entities.Unit;

public class CreatureAI {
	private Creature creature;
	private Unit target;
	private ThreatList threatList;
	private int aggroRange;
	
	public CreatureAI(Creature creature) {
		this.creature = creature;
		threatList = new ThreatList();
		aggroRange = 0;
	}
	
	public ThreatList getThreatList() {
		return threatList;
	}
	
	private void findTarget() {
		if (threatList.size() != 0)
			target = threatList.getUnitWithHighestThreat();
	}
	
	private DirectionFacing getTargetDirection() {
		if (target.getArea() > creature.getArea())
			return DirectionFacing.right;
		else if (target.getArea() < creature.getArea())
			return DirectionFacing.left;
		
		// same area
		if (target.getX() > creature.getX() + creature.getWidth()) {
			return DirectionFacing.right;
		} else if (target.getX() + target.getWidth() < creature.getX()) {
			return DirectionFacing.left;
		}
		return DirectionFacing.forward;
	}
	
	public void chaseTarget() {
		findTarget();
		if (target.getArea() == creature.getArea()) {
			DirectionFacing directionToMove = getTargetDirection();
			creature.moveInDirection(directionToMove);
			
			if (target.getY() > creature.getY())
				creature.jump();
		} else {
			creature.setDX(0);
		}
	}
	
	public void update() {
		switch (creature.getScript()) {
		case none:
			switch (creature.getAiType()) {
			case aggressive:
				break;
			case caster:
				findTarget();
				if (target != null) {
					creature.setDirectionFacing(getTargetDirection());
					if (!creature.isCasting())
						creature.castSpell(creature.getSpellBook().get("Fireball"));
				}
				break;
			case neutral:
				if (threatList.size() != 0) {
					chaseTarget();
				} else {
					creature.setDX(0);
				}
				break;
			case none:
				creature.setDX(0);
				break;
			case passive:
				creature.setDX(0);
				break;
			case passive_aggressive:
				
				break;
			default:
				creature.setDX(0);
				break;
			}
			break;
		default:
			creature.setDX(0);
			break;
		}
		
	}
}
