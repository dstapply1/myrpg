package myrpg.creatures;

import myrpg.items.Item;

public class LootEntry {
	private Item item;
	// 0.0000 - 1.0000
	private float dropChance;
	
	public LootEntry(Item item, float dropChance) {
		this.item = item;
		this.dropChance = dropChance;
	}
	
	public Item getItem() {
		return item;
	}
	
	public float getDropChance() {
		return dropChance;
	}
}
