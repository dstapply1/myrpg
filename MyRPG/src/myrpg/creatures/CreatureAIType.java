package myrpg.creatures;

public enum CreatureAIType {
	none,
	passive,
	neutral,
	passive_aggressive,
	aggressive,
	caster
}
