package myrpg.creatures;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashMap;

import myrpg.entities.DirectionFacing;
import myrpg.entities.Player;
import myrpg.entities.Unit;
import myrpg.items.Item;
import myrpg.spells.Spell;
import myrpg.spells.SpellDummy;
import myrpg.view.MyRPGFrame;

public class Creature extends Unit {
	private String name;
	private CreatureAIType aiType;
	private CreatureScript script;
	private CreatureAI ai;
	private LootTable lootTable;
	
	public Creature(String name, String spriteDirPath) {
		super(spriteDirPath);
		this.name = name;
		x = 600;
		y = 300;
		aiType = CreatureAIType.none;
		script = CreatureScript.none;
		ai = new CreatureAI(this);
		lootTable = new LootTable();
	}
	
	@Override
	public void move() {
		ai.update();
		if (dx != 0) {
			walking = true;
		} else {
			walking = false;
		}
		
		if (walking || dy != 0) {
			moving = true;
			if (casting)
				cancelSpellCast();
		} else {
			moving = false;
			return;
		}
		
		x += dx;
		y += dy;
		
		if (x < -1*(width/2)) {
			x = -1*(width/2);
		}
		
		if (x > MyRPGFrame.wWidth - width/2) {
			x = MyRPGFrame.wWidth - width/2;
		}
		
		if (y > MyRPGFrame.wHeight - height)
			y = MyRPGFrame.wHeight - height;
		
		if (y < -5)
			y = -5;
		
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public CreatureAIType getAiType() {
		return aiType;
	}

	public void setAiType(CreatureAIType aiType) {
		this.aiType = aiType;
	}

	public CreatureScript getScript() {
		return script;
	}

	public void setScript(CreatureScript script) {
		this.script = script;
	}
	
	public void addThreat(Unit fromUnit, float threat) {
		ai.getThreatList().addThreat(fromUnit, threat);
	}
	
	public void die(Player killer) {
		// state
		alive = false;
		if (casting) {
			cancelSpellCast();
		}
		
		// play death animation
		
		
		// Reward xp
		int xpGain = (int) (level + (rank*0.5));
		killer.gainExp(xpGain);
		
		// drop loot
		ArrayList<Item> loot = lootTable.rollForItems();
		for (Item item: loot) {
			dropItem(item);
		}
	}

	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
	}
	
	public void keyReleased(KeyEvent e) {
		int key = e.getKeyCode();
	}
}
