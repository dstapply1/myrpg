package myrpg.creatures;

import java.util.ArrayList;

import myrpg.entities.Unit;

public class ThreatList {
	private ArrayList<ThreatEntry> entries; 
	
	public ThreatList() {
		entries = new ArrayList<ThreatEntry>();
	}
	
	public void addThreat(Unit unit, float threat) {
		boolean hasUnit = false;
		for (int i = 0; i < entries.size(); i++) {
			ThreatEntry entry = entries.get(i);
			if (entry.getUnit().equals(unit)) {
				entry.setThreat(entry.getThreat() + threat);
				hasUnit = true;
			}
		}
		if (!hasUnit)
			entries.add(new ThreatEntry(unit, threat));
	}
	
	public Unit getUnitWithHighestThreat() {
		float highestThreat = 0;
		Unit unit = null;
		for (int i = 0; i < entries.size(); i++) {
			ThreatEntry entry = entries.get(i);
			if (entry.getThreat() > highestThreat) {
				highestThreat = entry.getThreat();
				unit = entry.getUnit();
			}
		}
		return unit;
	}
	
	public int size() {
		return entries.size();
	}
}
