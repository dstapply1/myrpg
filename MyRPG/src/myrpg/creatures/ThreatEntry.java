package myrpg.creatures;

import myrpg.entities.Unit;

public class ThreatEntry {
	private Unit unit;
	private float threat;
	
	public ThreatEntry(Unit unit, float threat) {
		this.unit = unit;
		this.threat = threat;
	}

	public Unit getUnit() {
		return unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	public float getThreat() {
		return threat;
	}

	public void setThreat(float threat) {
		this.threat = threat;
	}
}
