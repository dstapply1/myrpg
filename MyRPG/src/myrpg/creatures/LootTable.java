package myrpg.creatures;

import java.util.ArrayList;
import java.util.Random;

import myrpg.items.Item;

public class LootTable {
	private ArrayList<LootEntry> lootEntries;
	private Random rand;
	
	public LootTable() {
		lootEntries = new ArrayList<LootEntry>();
		rand = new Random();
	}
	
	public ArrayList<Item> rollForItems() {
		ArrayList<Item> rolledItems = new ArrayList<Item>();
		for (LootEntry lootEntry: lootEntries) {
			float chance = lootEntry.getDropChance();
			float roll = randInt(0, 10000)/10000.0f;
			if (chance > roll) {
				// roll success
				rolledItems.add(lootEntry.getItem());
			}
		}
		return rolledItems;
	}
	
	public int randInt(int min, int max) {
	    int randomNum = rand.nextInt((max - min) + 1) + min;

	    return randomNum;
	}
}
