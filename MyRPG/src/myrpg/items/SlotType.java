package myrpg.items;

public enum SlotType {
	// Gear Slots
	mainhand,
	offhand,
	head,
	shoulder,
	chest,
	hands,
	legs,
	feet,
	back,
	neck,
	ring1,
	ring2,
	backpack,
	maxGearSlots,
	// other
	backpackSlot
}
