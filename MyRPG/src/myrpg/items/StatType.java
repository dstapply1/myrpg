package myrpg.items;

public enum StatType {
	bonusHealth,
	bonusResource,
	stamina,
	strength,
	agility,
	intellect,
	spririt,
	attackPower,
	spellPower,
	maxStatType
}
