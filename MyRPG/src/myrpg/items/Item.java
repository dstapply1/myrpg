package myrpg.items;

import java.util.ArrayList;

import javax.swing.Icon;

import myrpg.sprites.Sprite;

public class Item {
	private String name;
	private String description;
	private Icon imageIcon;
	private Sprite sprite;
	private int[] damageRange;
	private ItemInventoryType type;
	private ArrayList<Stat> stats;
	private int slotCount;
	
	public Item(String name, ItemInventoryType type, ArrayList<Stat> stats) {
		damageRange = new int[2];
		damageRange[0] = 0;
		damageRange[1] = 0;
		this.name = name;
		this.type = type;
		this.stats = stats;
		slotCount = 0;
	}
	
	public String getName() {
		return name;
	}
	
	public ItemInventoryType getItemInventoryType() {
		return type;
	}
	
	public int getSlotCount() {
		return slotCount;
	}
	
	public void setSlotCount(int slotCount) {
		this.slotCount = slotCount;
	}
	
	public ArrayList<Stat> getStats() {
		return stats;
	}
}
