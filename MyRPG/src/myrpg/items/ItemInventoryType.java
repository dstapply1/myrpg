package myrpg.items;

public enum ItemInventoryType {
	mainHand,
	offHand,
	oneHand,
	twoHand,
	head,
	shoulder,
	chest,
	hands,
	legs,
	feet,
	back,
	neck,
	ring,
	bag
}
