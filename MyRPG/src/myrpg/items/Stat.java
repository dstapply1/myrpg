package myrpg.items;

import java.util.ArrayList;

public class Stat {
	private StatType type;
	private int value;
	
	public Stat(StatType type, int value) {
		this.type = type;
		this.value = value;
	}
	
	public StatType getType() {
		return type;
	}
	
	public int getValue() {
		return value;
	}
}
