package myrpg.items;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.ImageIcon;

import myrpg.entities.Player;
import myrpg.sprites.Sprite;
import myrpg.sprites.SpriteType;

public class Inventory {
	private Player player;
	private int capacity;
	private ArrayList<InventorySlot> slots;
	private Sprite sprite;
	
	public Inventory(Player player) {
		this.player = player;
		capacity = 0;
		slots = new ArrayList<InventorySlot>();
		
		// Gear slots
		addSlot(SlotType.mainhand);
		addSlot(SlotType.offhand);
		addSlot(SlotType.head);
		addSlot(SlotType.shoulder);
		addSlot(SlotType.chest);
		addSlot(SlotType.hands);
		addSlot(SlotType.legs);
		addSlot(SlotType.feet);
		addSlot(SlotType.back);
		addSlot(SlotType.neck);
		addSlot(SlotType.ring1);
		addSlot(SlotType.ring2);
		addSlot(SlotType.backpack);
		
		// regular slots
		for (int i = 0; i < 16; i++) {
			addSlot(SlotType.backpackSlot);
		}
		
		ImageIcon bg = new ImageIcon("images/sprites/UI/Inventory/Inventory.png");
		sprite = new Sprite(bg.getImage(), SpriteType.UI);
	}
	
	public ArrayList<InventorySlot> getSlots() {
		return slots;
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public void addSlot(SlotType type) {
		InventorySlot slot = new InventorySlot(this, type);
		slots.add(capacity++, slot);
	}
	
	public void removeSlot(int index) {
		if (!slots.get(index).isEmpty()) {
			// TODO drop item
			slots.get(index).getItem();
		}
		slots.remove(index);
		capacity--;
	}
	
	public boolean contains(Item item) {
		for (int i = 0; i < slots.size(); i++) {
			if (slots.get(i).getItem().equals(item))
				return true;
		}
		return false;
	}
	
	public void addBagSlots(Item bag) {
		for (int i = 0; i < bag.getSlotCount(); i++) {
			addSlot(SlotType.backpackSlot);
		}
	}
	
	public void removeBagSlots(Item bag) {
		for (int i = 0; i < bag.getSlotCount(); i++) {
			removeSlot(capacity);
		}
	}
	
	public boolean addItem(Item item) {
		for (int i = 0; i < slots.size(); i++) {
			InventorySlot slot = slots.get(i);
			if (slot.isEmpty()) {
				switch (slot.getSlotType()) {
				case back:
					if (item.getItemInventoryType() == ItemInventoryType.back) {
						slot.addItem(item);
						return true;
					}
					break;
				case backpackSlot:
					slot.addItem(item);
					return true;
				case backpack:
					if (item.getItemInventoryType() == ItemInventoryType.bag) {
						slot.addItem(item);
						return true;
					}
					break;
				case chest:
					if (item.getItemInventoryType() == ItemInventoryType.chest) {
						slot.addItem(item);
						return true;
					}
					break;
				case feet:
					if (item.getItemInventoryType() == ItemInventoryType.feet) {
						slot.addItem(item);
						return true;
					}
					break;
				case hands:
					if (item.getItemInventoryType() == ItemInventoryType.hands) {
						slot.addItem(item);
						return true;
					}
					break;
				case head:
					if (item.getItemInventoryType() == ItemInventoryType.head) {
						slot.addItem(item);
						return true;
					}
					break;
				case legs:
					if (item.getItemInventoryType() == ItemInventoryType.legs) {
						slot.addItem(item);
						return true;
					}
					break;
				case mainhand:
					if (item.getItemInventoryType() == ItemInventoryType.mainHand
							|| item.getItemInventoryType() == ItemInventoryType.oneHand
							|| (item.getItemInventoryType() == ItemInventoryType.twoHand && slots.get(SlotType.offhand.ordinal()).isEmpty())) {
						slot.addItem(item);
						return true;
					}
					break;
				case neck:
					if (item.getItemInventoryType() == ItemInventoryType.neck) {
						slot.addItem(item);
						return true;
					}
					break;
				case offhand:
					if (item.getItemInventoryType() == ItemInventoryType.offHand
							|| item.getItemInventoryType() == ItemInventoryType.oneHand) {
						slot.addItem(item);
						return true;
					}
					break;
				case ring1:
					if (item.getItemInventoryType() == ItemInventoryType.ring) {
						slot.addItem(item);
						return true;
					}
					break;
				case ring2:
					if (item.getItemInventoryType() == ItemInventoryType.ring) {
						slot.addItem(item);
						return true;
					}
					break;
				case shoulder:
					if (item.getItemInventoryType() == ItemInventoryType.shoulder) {
						slot.addItem(item);
						return true;
					}
					break;
				default:
					break;
				}
			}
		}
		
		System.out.println("Inventory Full");
		
		return false;
	}
	
	public void applyGearStats() {
		for (InventorySlot slot: slots) {
			slot.applyItemStats();
		}
	}
	
	public void removeGearStats() {
		for (InventorySlot slot: slots) {
			slot.removeItemStats();
		}
	}
	
	public Image getImage() {
		return sprite.getCurrentImage();
	}
	
	public void keyReleased(KeyEvent e) {
		player.keyReleased(e);
	}
	
	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		
		if (key == KeyEvent.VK_I) {
			player.setInInventory(false);
		}
	}
}