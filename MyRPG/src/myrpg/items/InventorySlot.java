package myrpg.items;

import myrpg.entities.Player;

public class InventorySlot {
	private Inventory inventory;
	private SlotType type;
	private boolean empty;
	private Item item;
	
	public InventorySlot(Inventory inventory, SlotType type) {
		this.inventory = inventory;
		this.type = type;
		empty = true;
		item = null;
	}
	
	public void addItem(Item item) {
		if (empty) {
			if (itemCanFillSlot(item)) {
				this.item = item;
				empty = false;
				if (isEquipSlot())
					applyItemStats();
			} else {
				// cant go into this slot
			}
		} else {
			System.out.println("Slot full.");
		}
	}
	
	public void removeItem() {
		removeItemStats();
		item = null;
		empty = true;
	}
	
	private boolean isEquipSlot() {
		if (type.ordinal() < SlotType.maxGearSlots.ordinal())
			return true;
		
		return false;
	}
	
	public void applyItemStats() {
		if (!empty && type != SlotType.backpackSlot) {
			if (type == SlotType.backpack)
				inventory.addBagSlots(item);
			
			Player player = inventory.getPlayer();
			for (int j = 0; j < item.getStats().size(); j++) {
				Stat stat = item.getStats().get(j);
				player.getItemStats().put(stat.getType(), player.getItemStats().get(stat.getType()) + stat.getValue());
			}
		}
	}
	
	public void removeItemStats() {
		if (!empty && type != SlotType.backpackSlot) {
			if (type == SlotType.backpack)
				inventory.removeBagSlots(item);
			
			Player player = inventory.getPlayer();
			for (int j = 0; j < item.getStats().size(); j++) {
				Stat stat = item.getStats().get(j);
				player.getItemStats().put(stat.getType(), player.getItemStats().get(stat.getType()) - stat.getValue());
			}
		}
	}
	
	public boolean itemCanFillSlot(Item item) {
		switch (item.getItemInventoryType()) {
		case back:
			if (type == SlotType.back)
				return true;
			break;
		case bag:
			if (type == SlotType.backpack)
				return true;
			break;
		case chest:
			if (type == SlotType.chest)
				return true;
			break;
		case feet:
			if (type == SlotType.feet)
				return true;
			break;
		case hands:
			if (type == SlotType.hands)
				return true;
			break;
		case head:
			if (type == SlotType.head)
				return true;
			break;
		case legs:
			if (type == SlotType.legs)
				return true;
			break;
		case mainHand:
			if (type == SlotType.mainhand)
				return true;
			break;
		case neck:
			if (type == SlotType.neck)
				return true;
			break;
		case offHand:
			if (type == SlotType.offhand)
				return true;
			break;
		case oneHand:
			if (type == SlotType.offhand || type == SlotType.mainhand)
				return true;
			break;
		case ring:
			if (type == SlotType.ring1 || type == SlotType.ring2)
				return true;
			break;
		case shoulder:
			if (type == SlotType.shoulder)
				return true;
			break;
		case twoHand:
			if (type == SlotType.mainhand)
				return true;
			break;
		default:
			break;
		}
		if (type == SlotType.backpackSlot)
			return true;
		
		return false;
	}
	
	public Item getItem() {
		if (empty) {
			System.out.println("No item in slot");
		}
		return item;
	}
	
	public boolean isEmpty() {
		return empty;
	}
	
	public SlotType getSlotType() {
		return type;
	}
}
