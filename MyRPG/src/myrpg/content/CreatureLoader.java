package myrpg.content;

import java.util.ArrayList;

import myrpg.creatures.Creature;
import myrpg.creatures.CreatureAIType;
import myrpg.locations.GameMapId;
import myrpg.spells.Spell;
import myrpg.spells.SpellCostType;

public class CreatureLoader {
	
	private static CreatureLoader instance = null;
	private static ArrayList<Creature> creatures;
	
	private CreatureLoader() {
		// load player and set collision
		creatures = new ArrayList<Creature>();
		loadCreatures();
	}
	
	public static CreatureLoader getInstance() {
		if (instance == null)
			instance = new CreatureLoader();
		return instance;
	}
	
	public ArrayList<Creature> getCreatures() {
		return creatures;
	}

	private void loadCreatures() {
		
		// Creature 1
		Creature testCreature = new Creature("Test", "images/sprites/characters/sticky");
		testCreature.setMapId(GameMapId.test1);
		testCreature.setArea(1);
		testCreature.setLevel(4);
		testCreature.setResourceType(SpellCostType.mana);
		testCreature.setAiType(CreatureAIType.caster);
		testCreature.setMovementSpeed(testCreature.getMovementSpeed()-4);
		// load spellbook
		Spell fireBall = new Spell(SpellLoader.getSpells().get("Fireball"), testCreature);
		testCreature.learnSpell(fireBall);
		
		creatures.add(testCreature);
	}
}
