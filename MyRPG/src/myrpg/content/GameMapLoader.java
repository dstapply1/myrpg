package myrpg.content;

import java.util.ArrayList;
import java.util.HashMap;

import myrpg.entities.Game_Object;
import myrpg.locations.GameArea;
import myrpg.locations.GameMap;
import myrpg.spells.Spell;

public class GameMapLoader {
	
	private static GameMapLoader instance = null;
	private static ArrayList<GameMap> maps;
	
	private GameMapLoader() {
		maps = new ArrayList<GameMap>();
		loadMaps();
	}
	
	public static GameMapLoader getInstance() {
		if (instance == null)
			instance = new GameMapLoader();
		return instance;
	}
	
	public ArrayList<GameMap> getMaps() {
		return maps;
	}
	
	private void loadMaps() {
		HashMap<String, Spell> spells = SpellLoader.getSpells();
		
		////////////////////////////////////////////
		// Map 1
		////////////////////////////////////////////
		GameMap map1 = new GameMap("Test");
		
		int areaId = 0;
		GameArea area1 = new GameArea(areaId++, "Starting Area");

		// Area 1
		Game_Object floor = new Game_Object("images/sprites/environment/floor/", -200, 640, true, true);
		Game_Object rock = new Game_Object("images/sprites/environment/rock/", 110, 480, true, true);
		Game_Object healthPack = new Game_Object("images/sprites/environment/healthpack/", 900, 580, true, true);
		healthPack.setAvoidSpellCollision(true);
		healthPack.setSpell(spells.get("Lesser Heal"));
		
		area1.addGameObject(floor);
		area1.addGameObject(rock);
		area1.addGameObject(healthPack);

		map1.addArea(area1);
		
		// Area 2
		GameArea area2 = new GameArea(areaId++, "Healing Zone");
		
		Game_Object floor2 = new Game_Object("images/sprites/environment/floor/", -200, 640, true, true);
		Game_Object healthPack2 = new Game_Object("images/sprites/environment/healthpack/", 800, 580, true, true);
		healthPack2.setAvoidSpellCollision(true);
		healthPack2.setSpell(spells.get("Lesser Heal"));
		Game_Object healthPack3 = new Game_Object("images/sprites/environment/healthpack/", 700, 580, true, true);
		healthPack3.setAvoidSpellCollision(true);
		healthPack3.setSpell(spells.get("Lesser Heal"));
		Game_Object healthPack4 = new Game_Object("images/sprites/environment/healthpack/", 600, 580, true, true);
		healthPack4.setAvoidSpellCollision(true);
		healthPack4.setSpell(spells.get("Lesser Heal"));
		Game_Object healthPack5 = new Game_Object("images/sprites/environment/healthpack/", 500, 580, true, true);
		healthPack5.setAvoidSpellCollision(true);
		healthPack5.setSpell(spells.get("Lesser Heal"));
		Game_Object healthPack6 = new Game_Object("images/sprites/environment/healthpack/", 400, 580, true, true);
		healthPack6.setAvoidSpellCollision(true);
		healthPack6.setSpell(spells.get("Lesser Heal"));
		
		area2.addGameObject(floor2);
		area2.addGameObject(healthPack2);
		area2.addGameObject(healthPack3);
		area2.addGameObject(healthPack4);
		area2.addGameObject(healthPack5);
		area2.addGameObject(healthPack6);
		
		map1.addArea(area2);
		
		maps.add(map1);
		
		//////////////////////////////////////////////////
		// Map 2
		//////////////////////////////////////////////////
	}
	
}
