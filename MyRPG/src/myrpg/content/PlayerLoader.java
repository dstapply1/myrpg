package myrpg.content;

import myrpg.entities.Player;
import myrpg.locations.GameMapId;
import myrpg.spells.Spell;
import myrpg.spells.SpellCostType;

public class PlayerLoader {
	
	private static PlayerLoader instance = null;
	private static Player player;
	
	private PlayerLoader() {
		// load player and set collision
		player = new Player("images/sprites/characters/sticky", "Alfeey");
		loadPlayerInfo();
	}
	
	public static PlayerLoader getInstance() {
		if (instance == null)
			instance = new PlayerLoader();
		return instance;
	}
	
	public Player getPlayer() {
		return player;
	}

	private void loadPlayerInfo() {
		player.setMapId(GameMapId.test1);
		player.setArea(0);
		player.setResourceType(SpellCostType.mana);
		// load spellbook
		Spell fireBall = new Spell(SpellLoader.getSpells().get("Fireball"), player);
		player.learnSpell(fireBall);
	}
}
