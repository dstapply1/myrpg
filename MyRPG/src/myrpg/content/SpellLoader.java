package myrpg.content;

import java.util.HashMap;

import myrpg.spells.Spell;
import myrpg.spells.SpellCostType;
import myrpg.spells.SpellDamageType;
import myrpg.spells.SpellDummy;
import myrpg.spells.SpellEffect;
import myrpg.spells.SpellEffectType;
import myrpg.sprites.Sprite;
import myrpg.sprites.SpriteType;

public class SpellLoader {
	private static SpellLoader instance = null;
	private static HashMap<String, Sprite> sprites;
	private static HashMap<String, Spell> spells;
	
	private SpellLoader() {
		sprites = new HashMap<String, Sprite>();
		spells = new HashMap<String, Spell>();
		loadSpells();
	}
	
	public static SpellLoader getInstance() {
		if (instance == null)
			instance = new SpellLoader();
		return instance;
	}
	
	public static HashMap<String, Spell> getSpells() {
		return spells;
	}
	
	public static HashMap<String, Sprite> getSprites() {
		return sprites;
	}
	
	private void loadSpells() {
		// load spell sprites
		sprites.put("Fireball", new Sprite("images/sprites/attacks/fireball/", null, SpriteType.spell));
		
		// load spells
		int spellId = 0;
		spells.put("Lesser Heal", new Spell(spellId++, "Lesser Heal", SpellCostType.none, 0, 0, null, null, null));
		SpellEffect fireBallEffect = new SpellEffect("Fireball Damage", "Explosion of fire dealing damage.", SpellEffectType.effect_damage_direct, SpellDamageType.damage_fire, 20, 0);
		SpellDummy fireBallDummy = new SpellDummy(sprites.get("Fireball"), true);
		Spell fireBall = new Spell(spellId++, "Fireball", SpellCostType.mana, 20, 500, fireBallDummy, fireBallEffect, null);
		spells.put("Fireball", fireBall);
	}
}
