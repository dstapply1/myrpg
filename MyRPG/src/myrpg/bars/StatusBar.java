package myrpg.bars;

import java.awt.Rectangle;

public class StatusBar {
	
	public StatusBar() {}
	
	public static Rectangle getRectangleFill(float current, float max, int width, int height) {
		float percentFilled = (current/max);
		if (percentFilled < 0) 
			percentFilled = 0;
		else if (percentFilled > 1)
			percentFilled = 1;
		
		return new Rectangle((int) (width*percentFilled), height);
	}
	
	
}
