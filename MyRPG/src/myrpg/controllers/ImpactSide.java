package myrpg.controllers;

public enum ImpactSide {
	none,
	top,
	bot,
	left,
	right
}
