package myrpg.controllers;

import java.awt.Rectangle;
import java.awt.geom.Line2D;
import java.util.ArrayList;

import myrpg.creatures.Creature;
import myrpg.entities.DirectionFacing;
import myrpg.entities.Game_Object;
import myrpg.entities.Player;
import myrpg.entities.Unit;
import myrpg.locations.GameArea;
import myrpg.locations.GameMap;
import myrpg.spells.SpellDummy;

public class CollisionController {
	
	private static CollisionController instance;
	
	private Player player;
	private ArrayList<Game_Object> gameObjects;
	private ArrayList<Creature> creatures;

	private CollisionController() {
		// player initially null, have to set
		player = null;
		gameObjects = new ArrayList<Game_Object>();
		creatures = new ArrayList<Creature>();
	}
	
	public static CollisionController getInstance() {
		if (instance == null) {
			instance = new CollisionController();
		}
		return instance;
	}
	
	public void addGameObject(Game_Object go) {
		gameObjects.add(go);
	}
	
	public void addCreature(Creature creature) {
		creatures.add(creature);
	}
	
	public void setCreatures(ArrayList<Creature> creatures) {
		this.creatures = creatures;
	}
		
	public void clearGameObjects() {
		gameObjects.clear();
	}
	
	public void setPlayer(Player p) {
		player = p;
	}
	
	public synchronized void checkCollisions() {
		checkPlayerGameObjectCollisions();
		checkCreatureGameObjectCollisions();
		checkPlayerSpellCollisions();
		checkCreatureSpellCollisions();
	}
	
	private void checkPlayerSpellCollisions() {
		// Ability Collisions
		ArrayList<SpellDummy> dummies = player.getSpellDummies();
		
		for (int j = 0; j < dummies.size(); j++) {
			SpellDummy dummy = (SpellDummy)dummies.get(j);
			
			// spell and go collision
			Rectangle dummyRec = dummy.getBounds();
			
			// Game object collisions
			for (int i = 0; i < gameObjects.size(); i++) {
				Game_Object go = gameObjects.get(i);
				if (go.isCollisionOn() && !go.isAvoidSpellCollisionOn()) {
					Rectangle goRec = go.getBounds();
					if (dummy.getArea() == go.getArea()) {
						if (dummyRec.intersects(goRec) && !dummy.isSpellEffectActive()) {
							//play spell collision animation
							dummy.setSpellEffectActive(true);
							if (dummy.getDirectionFacing() == DirectionFacing.left)
								dummy.setX(dummy.getX() - dummyRec.width/2);
							else if (dummy.getDirectionFacing() == DirectionFacing.right)
								dummy.setX(dummy.getX() + dummyRec.width/2);
						}
					}
				}
			}
			
			// creature collisions
			for (int k = 0; k < creatures.size(); k++) {
				Creature creature = creatures.get(k);
				
				if (creature.isAlive() && creature.getArea() == dummy.getArea()) {
					Rectangle creatureRec = creature.getBounds();
					
					if (dummyRec.intersects(creatureRec) && !dummy.isSpellEffectActive()) {
						//play missile collision animation
						dummy.setSpellEffectActive(true);
						if (dummy.getDirectionFacing() == DirectionFacing.left)
							dummy.setX(dummy.getX() - dummyRec.width/2);
						else if (dummy.getDirectionFacing() == DirectionFacing.right)
							dummy.setX(dummy.getX() + dummyRec.width/2);
						dummy.applyAffect(creature, dummy.getOwner());
					}
				}
			}
		}
	}
	
	private void checkCreatureSpellCollisions() {
		for (int l = 0; l < creatures.size(); l++) {
			Creature creature = creatures.get(l);
			// Ability Collisions
			ArrayList<SpellDummy> dummies = creature.getSpellDummies();
			
			for (int j = 0; j < dummies.size(); j++) {
				SpellDummy dummy = (SpellDummy)dummies.get(j);
				
				// spell and go colision
				Rectangle dummyRec = dummy.getBounds();
				
				// Game object collisions
				for (int i = 0; i < gameObjects.size(); i++) {
					Game_Object go = gameObjects.get(i);
					if (go.isCollisionOn() && !go.isAvoidSpellCollisionOn()) {
						Rectangle goRec = go.getBounds();
						if (dummy.getArea() == go.getArea()) {
							if (dummyRec.intersects(goRec) && !dummy.isSpellEffectActive()) {
								//play spell collision animation
								dummy.setSpellEffectActive(true);
								if (dummy.getDirectionFacing() == DirectionFacing.left)
									dummy.setX(dummy.getX() - dummyRec.width/2);
								else if (dummy.getDirectionFacing() == DirectionFacing.right)
									dummy.setX(dummy.getX() + dummyRec.width/2);
							}
						}
					}
				}
				
				if (dummy.getArea() == player.getArea()) {
					// collide with player
					Rectangle playerRec = player.getBounds();
					if (dummyRec.intersects(playerRec) && !dummy.isSpellEffectActive()) {
						//play spell collision animation
						dummy.setSpellEffectActive(true);
						if (dummy.getDirectionFacing() == DirectionFacing.left)
							dummy.setX(dummy.getX() - dummyRec.width/2);
						else if (dummy.getDirectionFacing() == DirectionFacing.right)
							dummy.setX(dummy.getX() + dummyRec.width/2);
						dummy.applyAffect(player, creature);
					}
				}
			}
		}
	}

	private void checkCreatureGameObjectCollisions() {
		//creature collides with object
		for (int k = 0; k < creatures.size(); k++) {
			boolean creatureOnObject = false;
			Creature creature = creatures.get(k);
			Rectangle creatureRec = creature.getBounds();
			
			// Game Object Collisions
			for (int i = 0; i < gameObjects.size(); i++) {
				Game_Object go = gameObjects.get(i);
				
				if (go.isCollisionOn()) {
					Rectangle goRec = gameObjects.get(i).getBounds();
					
					if (go.getArea() == creature.getArea()) {
						if (creatureRec.intersects(goRec)) {
							// if spell casted successfully, skip object
							if (applyGameObjectSpell(creature, go))
								continue;
							
							handleUnitGameObjectImpact(go, creature);
						} else {
							
						}
						
						// Check if Creature is standing on object
						if (!creature.isGravityActive()) {
							Rectangle testCreatureRec = creature.getBounds();
							testCreatureRec.setLocation(testCreatureRec.x, testCreatureRec.y + (int)(creature.GRAVITY_WEIGHT*2));
							if (testCreatureRec.intersects(goRec)) {
								creatureOnObject = true;
							} else {
								//System.out.println("Creature not on top of object");
							}
						}
					}
				}
			}
			
			if (!creatureOnObject) {
				//System.out.println("Creature falling");
				creature.setGravityActive(true);
			}
		}
	}
	
	private void checkPlayerGameObjectCollisions() {
		Rectangle playerRec = player.getBounds();
		boolean playerOnObject = false;
		
		// Game Object Collisions
		for (int i = 0; i < gameObjects.size(); i++) {
			Game_Object go = gameObjects.get(i);
			
			if (go.isCollisionOn() && go.isVisible()) {
				//player collides with object
				if (go.getArea() == player.getArea()) {
					Rectangle goRec = gameObjects.get(i).getBounds();
					
					if (playerRec.intersects(goRec)) {
						// if spell casted successfully, skip object
						if (applyGameObjectSpell(player, go))
							continue;
						
						handleUnitGameObjectImpact(go, player);
					} else {
						
					}
					
					// Check if player is standing on object
					if (!player.isGravityActive()) {
						Rectangle testPlayerRec = player.getBounds();
						testPlayerRec.setLocation(testPlayerRec.x, testPlayerRec.y + (int)(player.GRAVITY_WEIGHT*2));
						if (testPlayerRec.intersects(goRec)) {
							playerOnObject = true;
						} else {
							//System.out.println("Player not on top of object");
						}
					}
				}
			}
		}
		
		if (!playerOnObject) {
			player.setGravityActive(true);
		}
	}
	
	private void handleUnitGameObjectImpact(Game_Object go, Unit unit) {
		ImpactSide impactSide = findImpactSide(go, unit);
		
		switch(impactSide) {
		case bot:
			unit.setJumping(false);
			unit.setGravityActive(false);
			// set location on top of object
			unit.setDY(0);
			unit.setY(go.getY() - unit.getHeight());
			break;
		case left:
			unit.setX(go.getX() + go.getCurrentImage().getWidth(null));
			break;
		case none:
			break;
		case right:
			unit.setX(go.getX() - unit.getWidth());
			break;
		case top:
			unit.setDY(0);
			unit.setY(go.getY() + go.getCurrentImage().getHeight(null));
			unit.setJumping(true);
			unit.setGravityActive(true);
			unit.takeDamage(10);
			break;
		default:
			break;
		
		}
	}
	
	private ImpactSide findImpactSide(Game_Object go, Unit unit) {
		Rectangle goRec = go.getBounds();
		boolean intersectsTop = goRec.intersectsLine(unit.getX(), unit.getY(), unit.getX()+unit.getWidth(), unit.getY());
		boolean intersectsBot = goRec.intersectsLine(unit.getX(), unit.getY()+unit.getHeight(), unit.getX()+unit.getWidth(), unit.getY()+unit.getHeight());
		boolean intersectsLeft = goRec.intersectsLine(unit.getX(), unit.getY(), unit.getX(), unit.getY()+unit.getHeight());
		boolean intersectsRight = goRec.intersectsLine(unit.getX()+unit.getWidth(), unit.getY(), unit.getX()+unit.getWidth(), unit.getY()+unit.getHeight());
		
		boolean impactTop = false;
		boolean impactBot = false;
		boolean impactLeft = false;
		boolean impactRight = false;
		
		if ((intersectsTop && !intersectsBot && intersectsLeft && intersectsRight)
				|| (intersectsTop && !intersectsBot && intersectsLeft && !intersectsRight)
				|| (intersectsTop && !intersectsBot && !intersectsLeft && intersectsRight)) {
			impactTop = true;
		}
		if ((!intersectsTop && intersectsBot && intersectsLeft && intersectsRight)
				|| (!intersectsTop && intersectsBot && !intersectsLeft && intersectsRight)
				|| (!intersectsTop && intersectsBot && intersectsLeft && !intersectsRight)) {
			impactBot = true;;
		}
		if ((intersectsTop && intersectsBot && intersectsLeft && !intersectsRight)
				|| (intersectsTop && !intersectsBot && intersectsLeft && !intersectsRight)
				|| (!intersectsTop && intersectsBot && intersectsLeft && !intersectsRight)) {
			impactLeft = true;
		}
		if ((intersectsTop && intersectsBot && !intersectsLeft && intersectsRight)
				|| (intersectsTop && !intersectsBot && !intersectsLeft && intersectsRight)
				|| (!intersectsTop && intersectsBot && !intersectsLeft && intersectsRight)) {
			impactRight = true;
		}
		
		if (impactTop && impactRight) {
			Rectangle intersection = unit.getBounds().intersection(goRec);
			
			if (intersection.width > intersection.height) {
				return ImpactSide.top;
			} else {
				return ImpactSide.right;
			}
		} else if (impactTop && impactLeft) {
			Rectangle intersection = unit.getBounds().intersection(goRec);
			
			if (intersection.width > intersection.height) {
				return ImpactSide.top;
			} else {
				return ImpactSide.left;
			}
		} else if (impactBot && impactRight) {
			Rectangle intersection = unit.getBounds().intersection(goRec);
			
			if (intersection.width > intersection.height) {
				return ImpactSide.bot;
			} else {
				return ImpactSide.right;
			}
		} else if (impactBot && impactLeft) {
			Rectangle intersection = unit.getBounds().intersection(goRec);
			
			if (intersection.width > intersection.height) {
				return ImpactSide.bot;
			} else {
				return ImpactSide.left;
			}
		}
		
		if (impactTop) {
			return ImpactSide.top;
		} else if (impactBot) {
			return ImpactSide.bot;
		} else if (impactLeft) {
			return ImpactSide.left;
		} else if (impactRight) {
			return ImpactSide.right;
		}
		
		return ImpactSide.none;
	}
	
	/*
	 * Returns true on spell cast
	 * Spell should handle special collision
	 * and skip default collision
	 */
	private synchronized boolean applyGameObjectSpell(Unit unit, Game_Object go) {
		if (go.getSpell() != null) {
			if (go.getSpell().getName().equals("Lesser Heal")) {
				unit.takeDamage(-20);
				go.setVisible(false);
				gameObjects.remove(go);
				return true;
			} else {
				System.out.println("GO Spell not recognized.");
			}
		} else {}
		return false;
	}
	
	public void loadMapGameObjects(GameMap map) {
		gameObjects.clear();
		
		ArrayList<Game_Object> mapObjects = new ArrayList<Game_Object>();
		ArrayList<GameArea> areas = map.getAreas();
		for (int i = 0; i < areas.size(); i++) {
			mapObjects.addAll(areas.get(i).getGameObjects());
		}
		gameObjects.addAll(mapObjects);
	}
}
