package myrpg.spells;

public enum SpellEffectType {
	effect_none,
	effect_damage_direct,
	effect_damage_over_time,
	effect_heal_direct,
	effect_heal_over_time,
	effect_stun
}
