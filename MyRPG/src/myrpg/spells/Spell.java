/**
 * 
 */
package myrpg.spells;

import myrpg.entities.Unit;

/**
 * @author Dalton
 *
 */

public class Spell {
	private int id;
	private String name;
	private SpellCostType costType;
	private int cost;
	private int castTime;
	private SpellDummy dummy;
	private SpellEffect effect;
	private Unit caster;
	
	public Spell(int id, String name, SpellCostType costType, int cost, int castTime, SpellDummy dummy, SpellEffect effect, Unit caster) {
		this.id = id;
		this.name = name;
		this.costType = costType;
		this.cost = cost;
		this.castTime = castTime;
		this.dummy = dummy;
		this.effect = effect;
		this.caster = caster;
	}
	
	public Spell(Spell spell, Unit caster) {
		this.id = spell.id;
		this.name = spell.name;
		this.costType = spell.costType;
		this.cost = spell.cost;
		this.castTime = spell.castTime;
		this.dummy = spell.dummy;
		this.effect = spell.effect;
		this.caster = caster;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public SpellCostType getCostType() {
		return costType;
	}

	public void setCostType(SpellCostType costType) {
		this.costType = costType;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public SpellDummy getDummy() {
		return dummy;
	}

	public void setDummy(SpellDummy dummy) {
		this.dummy = dummy;
	}

	public SpellEffect getEffect() {
		return effect;
	}

	public void setEffect(SpellEffect effect) {
		this.effect = effect;
	}

	public int getCastTime() {
		return castTime;
	}

	public void setCastTime(int castTime) {
		this.castTime = castTime;
	}

	public Unit getCaster() {
		return caster;
	}

	public void setCaster(Unit caster) {
		this.caster = caster;
	}
	
	public SpellDummy getNewDummy() {
		return new SpellDummy(dummy, this);
	}
}
