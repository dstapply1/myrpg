package myrpg.spells;

public enum SpellCostType {
	none,
	mana,
	energy,
	health
}
