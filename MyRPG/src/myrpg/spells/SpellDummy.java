/**
 * 
 */
package myrpg.spells;

import myrpg.entities.DirectionFacing;
import myrpg.entities.Unit;
import myrpg.game.Game;
import myrpg.sprites.Sprite;
import myrpg.sprites.SpriteType;
import myrpg.view.MyRPGFrame;

/**
 * @author Dalton
 *
 */

public class SpellDummy extends Unit {
	private boolean spellEffectActive;
	private boolean moves;
	private boolean effectFinished;
	private Unit owner;
	private Spell spell;
	
	public SpellDummy(Sprite sprite, boolean moves) {
		super();
		spellEffectActive = false;
		visible = false;
		walking = true;
		this.moves = moves;
		this.sprite = sprite;
		effectFinished = false;
	}
	
	public SpellDummy(SpellDummy dummy, Spell spell) {
		super();
		spellEffectActive = false;
		visible = false;
		walking = true;
		moves = dummy.moves;
		sprite = dummy.sprite;
		effectFinished = false;
		this.spell = spell;
	}
	
	public void display(Unit caster) {
		this.y = caster.getY() + (caster.getCurrentImage().getHeight(null)/3);
		mapId = caster.getMapId();
		area = caster.getArea();
		facing = caster.getDirectionFacing();
		owner = caster;
		if (facing == DirectionFacing.right) {
			this.x = caster.getX() + caster.getCurrentImage().getWidth(null);
			dx = UNIT_SPEED;
		} else if (facing == DirectionFacing.left) {
			this.x = caster.getX() - caster.getCurrentImage().getWidth(null);
			dx = -UNIT_SPEED;
		}
		this.sprite = new Sprite(sprite, this, SpriteType.spell);
		visible = true;
	}
	
	public boolean isSpellEffectActive() {
		return spellEffectActive;
	}
	
	public void setSpellEffectActive(boolean val) {
		spellEffectActive = val;
	}
	
	public boolean getMoves() {
		return moves;
	}
	
	public boolean isEffectFinished() {
		return effectFinished;
	}
	
	public void setEffectFinished(boolean val) {
		effectFinished = val;
	}
	
	public Unit getOwner() {
		return owner;
	}
	
	@Override
	public void move() {
		if (moves && !spellEffectActive)
			x += dx;
		else return;
		
		if (x < -1*(width/2)) {
			if (Game.getCurrentMap().hasPreviousArea(area)) {
				area--;
				x = (int) (MyRPGFrame.wWidth - width/2);
			} else {
				spellEffectActive = true;
			}
		}
		
		if (x > MyRPGFrame.wWidth - width/2) {
			if (Game.getCurrentMap().hasNextArea(area)) {
				area++;
				x = -1*(width/2);
			} else
				spellEffectActive = true;
		}
		
		if (y > MyRPGFrame.wHeight - height)
			y = MyRPGFrame.wHeight - height;
		
		if (y < -5)
			y = -5;
	}

	public void applyAffect(Unit target, Unit fromUnit) {
		spell.getEffect().applyEffect(target, fromUnit);
	}
}
