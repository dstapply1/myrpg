package myrpg.spells;

public enum SpellDamageType {
	damage_physical,
	damage_fire,
	damage_frost,
	damage_arcane,
	damage_shadow,
	damage_light
}
