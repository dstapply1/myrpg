/**
 * 
 */
package myrpg.spells;

import myrpg.entities.Unit;

/**
 * @author Dalton
 *
 */

public class SpellEffect {
	private String name, description;
	private SpellEffectType effect;
	private SpellDamageType damageType;
	private int value, duration;
	
	public SpellEffect(String name, String description, 
			SpellEffectType effect, SpellDamageType damageType, int value, int duration) {
		this.name = name;
		this.description = description;
		this.effect = effect;
		this.damageType = damageType;
		this.value = value;
		this.duration = duration;
	}
	
	public void applyEffect(Unit target, Unit fromUnit) {
		switch (effect) {
		case effect_damage_direct:
			target.takeSpellDamage(this, fromUnit);
			break;
		default:
			break;
		}
		
		if (target != fromUnit) {
			try {
				// add threat to creature
				target.toCreature().addThreat(fromUnit, 1);
			} catch(Exception e) {
				// only throws if hit player
				//e.printStackTrace();
			}
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public SpellEffectType getEffect() {
		return effect;
	}

	public void setEffect(SpellEffectType effect) {
		this.effect = effect;
	}

	public SpellDamageType getDamageType() {
		return damageType;
	}

	public void setDamageType(SpellDamageType damageType) {
		this.damageType = damageType;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}
	
	
}
