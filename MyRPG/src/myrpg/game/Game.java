package myrpg.game;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JPanel;

import myrpg.bars.StatusBar;
import myrpg.content.CreatureLoader;
import myrpg.content.GameMapLoader;
import myrpg.content.PlayerLoader;
import myrpg.content.SpellLoader;
import myrpg.controllers.CollisionController;
import myrpg.creatures.Creature;
import myrpg.entities.Game_Object;
import myrpg.entities.Player;
import myrpg.entities.Unit;
import myrpg.locations.GameMap;
import myrpg.spells.Spell;
import myrpg.spells.SpellDummy;
import myrpg.view.Menu;
import myrpg.view.MyRPGFrame;

public class Game implements ActionListener {
	
	private JPanel panel;
	
	private static Player player;
	private boolean inGame;
	private Menu smenu, gmenu, gomenu;
	
	private static ArrayList<Game_Object> currentAreaGameObjects;
	private static ArrayList<Creature> creatures;
	private static HashMap<String, Spell> spells;
	
	private static ArrayList<GameMap> maps;
	
	private PlayerLoader playerLoader;
	private GameMapLoader gameMapLoader;
	private SpellLoader spellLoader;
	private CreatureLoader creatureLoader;
	
	private static CollisionController collisionController;
	
	private String framesPerSecond;
	private long beforeTime, currentTime;
	private int framesCount = 0;
	private int framesCountAvg = 0; 
	private long framesTimer = 0;
	private boolean fpsEnabled = true;
	
	public Game(JPanel panel) {
		this.panel = panel;
		initGame();
	}
	
	public void setPanel(JPanel panel) {
		this.panel = panel;
	}
	
	public boolean isInGame() {
		return inGame;
	}
	
	public void setInGame(boolean val) {
		inGame = val;
	}
	
	private void initGame() {
		
		currentAreaGameObjects = new ArrayList<Game_Object>();
		creatures = new ArrayList<Creature>();
		spells = new HashMap<String, Spell>();
		maps = new ArrayList<GameMap>();
		
		collisionController = CollisionController.getInstance();
		// Load spells first
		spellLoader = SpellLoader.getInstance();
		spells = SpellLoader.getSpells();
		// Load player
		playerLoader = PlayerLoader.getInstance();
		player = playerLoader.getPlayer();
		// Load Creatures
		creatureLoader = CreatureLoader.getInstance();
		creatures = creatureLoader.getCreatures();
		// Load Maps
		gameMapLoader = GameMapLoader.getInstance();
		maps = gameMapLoader.getMaps();
		
		smenu = new Menu(this);
		smenu.initStartMenu();
		gmenu = new Menu(this);
		gmenu.initInGameMenu();
		gomenu = new Menu(this);
		gomenu.initGOMenu();
		
		inGame = false;
		smenu.setInMenu(true);
		gmenu.setInMenu(false);
		gomenu.setInMenu(false);
		
		// Current map = map 1 for now
		currentAreaGameObjects = maps.get(player.getMapId().ordinal()).getAreas().get(player.getArea()).getGameObjects();
		
		// Set collisions
		collisionController.setPlayer(player);
		collisionController.loadMapGameObjects(getCurrentMap());
		collisionController.setCreatures(creatures);
	}
	
	private void drawInGame(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
	    
	    for (int i = 0; i < currentAreaGameObjects.size(); i++) {
	    	Game_Object go = currentAreaGameObjects.get(i);
	    	if (go.isVisible())
	    		g2d.drawImage(go.getCurrentImage(), go.getX(), go.getY(), panel);
	    }
				
		if (player.isVisible()) {
			g2d.drawImage(player.getCurrentImage(), player.getX(), player.getY(), panel);
			//drawHealthBar(g, player);
			player.displayStatusChanges(g);
			
			//Draw spells
			ArrayList<SpellDummy> dummies = player.getSpellDummies();
		
			for (int i = 0; i < dummies.size(); i++) {
				SpellDummy dummy = (SpellDummy) dummies.get(i);
				dummy.updateSprite();
				if (dummy.getArea() == player.getArea())
					dummy.setVisible(true);
				else
					dummy.setVisible(false);
				if (dummy.isVisible()) {
					g2d.drawImage(dummy.getCurrentImage(), dummy.getX(), dummy.getY(), panel);
				}
			}
		}
		
		for (int i = 0; i < creatures.size(); i++) {
			Creature creature = creatures.get(i);
			if (creature.isVisible() && creature.getArea() == player.getArea() && creature.isAlive()) {
				g2d.drawImage(creature.getCurrentImage(), creature.getX(), creature.getY(), panel);
				drawCreatureInfo(g, creature);
				drawHealthBar(g, creature);
				creature.displayStatusChanges(g);
			}
			
			//Draw spells
			ArrayList<SpellDummy> dummies = creature.getSpellDummies();
		
			for (int j = 0; j < dummies.size(); j++) {
				SpellDummy dummy = (SpellDummy) dummies.get(j);
				dummy.updateSprite();
				if (dummy.getArea() == player.getArea())
					dummy.setVisible(true);
				else
					dummy.setVisible(false);
				if (dummy.isVisible()) {
					g2d.drawImage(dummy.getCurrentImage(), dummy.getX(), dummy.getY(), panel);
				}
			}
		}
		
		// Print Player Health
		String hp;
		int hpX = 50;
		int hpY = 50;
		if (player.isAlive()) {
			hp = "Health: ";
			drawString(g, hp + player.getCurHealth(), hpX, hpY, 20, Color.WHITE);
		} else {
			hp = "Dead";
			drawString(g, hp, hpX, hpY, 20, Color.WHITE);
			inGame = false;
			gomenu.setInMenu(true);
		}
		
		// Print Player resource
		String resource;
		int rX = 50;
		int rY = 75;
		switch(player.getResourceType()) {
		case none:
			resource = "";
			break;
		case energy:
			resource = "Energy: " + player.getResourceValue();
			drawString(g, resource, rX, rY, 20, Color.yellow);
			break;
		case health:
			resource = "";
			System.out.println("Shouldn't get called...");
			break;
		case mana:
			resource = "Mana: " + player.getResourceValue();
			drawString(g, resource, rX, rY, 20, Color.blue);
			break;
		default:
			resource = "";
			break;
		}
		
		// Draw level
		drawString(g, "Level: " + player.getLevel(), 50, 100, 20, Color.white);
		
		// Draw player xp bar
		drawPlayerExpBar(g, player);
		
		// Draw player cast bar
		drawPlayerCastBar(g, player);
		
		// draw ui last
		drawUI(g, player);
	}
	
	private void drawMenus(Graphics g) {
		if (gmenu.isInMenu()) {
			Font small = new Font("Helvetica", Font.BOLD, 36);
			FontMetrics metr = panel.getFontMetrics(small);
			
			g.setColor(Color.WHITE);
			g.setFont(small);
			
			for (int i = 0; i < gmenu.getMenu().size(); i++) {
				if (gmenu.getMenu().get(i).isSelected()) {
					g.setColor(Color.YELLOW);
					g.drawString(gmenu.getMenu().get(i).getText(), (panel.getWidth() - metr.stringWidth(gmenu.getMenu().get(i).getText())) / 2, (panel.getHeight() - ((metr.getHeight() + 20) * (i+1))) / 2);
					g.setColor(Color.WHITE);
				} else {
					g.drawString(gmenu.getMenu().get(i).getText(), (panel.getWidth() - metr.stringWidth(gmenu.getMenu().get(i).getText())) / 2, (panel.getHeight() - ((metr.getHeight() + 20) * (i+1))) / 2);
				}
			}
		} else if (gomenu.isInMenu()) {
			String msg = "Game Over";
			Font small = new Font("Helvetica", Font.BOLD, 36);
			FontMetrics metr = panel.getFontMetrics(small);
			
			g.setColor(Color.RED);
			g.setFont(small);
			g.drawString(msg, (panel.getWidth() - metr.stringWidth(msg)) / 2, panel.getHeight() / 3);
			
			g.setColor(Color.WHITE);
			g.setFont(small);
			
			for (int i = 0; i < gomenu.getMenu().size(); i++) {
				if (gomenu.getMenu().get(i).isSelected()) {
					g.setColor(Color.YELLOW);
					g.drawString(gomenu.getMenu().get(i).getText(), (panel.getWidth() - metr.stringWidth(gomenu.getMenu().get(i).getText())) / 2, (panel.getHeight() - ((metr.getHeight() + 20) * (i+1))) / 2);
					g.setColor(Color.WHITE);
				} else {
					g.drawString(gomenu.getMenu().get(i).getText(), (panel.getWidth() - metr.stringWidth(gomenu.getMenu().get(i).getText())) / 2, (panel.getHeight() - ((metr.getHeight() + 20) * (i+1))) / 2);
				}
			}
		} else if (smenu.isInMenu()) {
			Font small = new Font("Helvetica", Font.BOLD, 36);
			FontMetrics metr = panel.getFontMetrics(small);
			
			g.setColor(Color.WHITE);
			g.setFont(small);
			
			for (int i = 0; i < smenu.getMenu().size(); i++) {
				if (smenu.getMenu().get(i).isSelected()) {
					g.setColor(Color.YELLOW);
					g.drawString(smenu.getMenu().get(i).getText(), (panel.getWidth() - metr.stringWidth(smenu.getMenu().get(i).getText())) / 2, (panel.getHeight() - ((metr.getHeight() + 20) * (i+1))) / 2);
					g.setColor(Color.WHITE);
				} else {
					g.drawString(smenu.getMenu().get(i).getText(), (panel.getWidth() - metr.stringWidth(smenu.getMenu().get(i).getText())) / 2, (panel.getHeight() - ((metr.getHeight() + 20) * (i+1))) / 2);
				}
			}
		} else {
			System.out.println("Something went wrong with the game state.");
			System.out.println("When trying to drawing graphics");
		}
	}
	
	public static void drawString(Graphics g, String string, int x, int y, int fontSize, Color color) {
		Font font = new Font("Helvetica", Font.BOLD, fontSize);
		
		g.setColor(color);
		g.setFont(font);
		g.drawString(string, x, y);
		g.setColor(Color.black);
	}
	
	private void drawHealthBar(Graphics g, Unit unit) {
		Graphics2D g2d = (Graphics2D) g;
		
		int healthBarWidth = unit.getWidth() + unit.getWidth()/6;
		int healthBarHeight = healthBarWidth/6;
		int healthBarX = unit.getX() - unit.getWidth()/12;
		int healthBarY = unit.getY() - unit.getHeight()/6;
		g2d.setColor(Color.green);
		g2d.drawRect(healthBarX, healthBarY, healthBarWidth, healthBarHeight);
		Rectangle healthBar = StatusBar.getRectangleFill(unit.getCurHealth(), unit.getMaxHealth(), healthBarWidth, healthBarHeight);
		g2d.fillRect(healthBarX, healthBarY, healthBar.width, healthBar.height);
		g2d.setColor(Color.black);
	}
	
	private void drawPlayerCastBar(Graphics g, Player player) {
		if (player.isCasting()) {
			Graphics2D g2d = (Graphics2D) g;
			
			int castBarWidth = player.getWidth() + player.getWidth()/6;
			int castBarHeight = castBarWidth/6;
			int castBarX = player.getX() - player.getWidth()/12;
			int castBarY = player.getY() - player.getHeight()/6;
			g2d.setColor(Color.white);
			g2d.drawRect(castBarX, castBarY, castBarWidth, castBarHeight);
			Rectangle castBar = StatusBar.getRectangleFill(player.getTimeCasting(), player.getSpellCasting().getCastTime(), castBarWidth, castBarHeight);
			g2d.fillRect(castBarX, castBarY, castBar.width, castBar.height);
			g2d.setColor(Color.black);
		}
	}
	
	private void drawCreatureInfo(Graphics g, Creature creature) {
		Graphics2D g2d = (Graphics2D) g;
		
		int x = creature.getX() - creature.getWidth()/12;
		int y = creature.getY() - creature.getHeight()/6 - 40;
		Color color = Color.white;
		int levelDiff = creature.getLevel() - player.getLevel();
		
		if (levelDiff > 3) {
			color = Color.red;
		} else if (levelDiff == 3) {
			color = Color.orange;
		} else if (levelDiff == 2) {
			color = Color.yellow;
		} else if (levelDiff <= 1 && levelDiff >= -1) {
		} else if (levelDiff ==  -2 || levelDiff == -3) {
			color = Color.green;
		} else if (levelDiff < -3) {
			color = Color.darkGray;
		}
		
		drawString(g, creature.getName(), x, y, 16, color);
		drawString(g, "Lvl: " + creature.getLevel(), x, y + 20, 16, color);
	}
	
	private void drawPlayerExpBar(Graphics g, Player player) {
		Graphics2D g2d = (Graphics2D) g;
		
		int x = 50;
		int y = 110;
		int width = 150;
		int height = 20;
		drawString(g, "Exp: ", x, y + 15, 20, Color.white);
		g2d.setColor(Color.magenta);
		g2d.drawRect(x + 50, y, width, height);
		Rectangle xpBar = StatusBar.getRectangleFill(player.getCurExp(), player.getExpToNextLevel(), width, height);
		g2d.fillRect(x + 50, y, xpBar.width, xpBar.height);
		g2d.setColor(Color.black);
	}
	
	private void drawUI(Graphics g, Player player) {
		Graphics2D g2d = (Graphics2D) g;
		
		if (player.isAlive()) {
			if (player.isInInventory()) {
				g2d.drawImage(player.getInventory().getImage(), 350, 150, panel);
			}
		}
	}

	public void draw(Graphics g) {
		//Record the time before update and draw
        beforeTime = System.nanoTime();
		
		if (inGame && !gmenu.isInMenu() && !gomenu.isInMenu() && !smenu.isInMenu()) {
			drawInGame(g);
		} else {
			drawMenus(g);
		}
		
		currentTime = System.currentTimeMillis(); 
		if (fpsEnabled) {
			framesPerSecond = "FPS: " + framesCountAvg;
	        drawString(g, framesPerSecond, 0, 20, 15, Color.YELLOW);
		}
        framesCount++; 
        if (currentTime - framesTimer > 1000) { 
			framesTimer = currentTime; 
			framesCountAvg = framesCount; 
			framesCount = 0;
        }
		
 		Toolkit.getDefaultToolkit().sync();
		g.dispose();
		
	}
	
	public void actionPerformed(ActionEvent e) {
		// Handle In game animations
		if (inGame) {
			if (player.isAlive()) {
			
				ArrayList<SpellDummy> dummies = player.getSpellDummies();
				
				for (int i = 0; i < dummies.size(); i++) {
					SpellDummy dummy = (SpellDummy) dummies.get(i);
					if (!dummy.isEffectFinished())
						dummy.update();
					else
						dummies.remove(dummy);
				}
				player.update();
				for (int i = 0; i < creatures.size(); i++) {
					Creature creature = creatures.get(i);
					if (creature.isAlive()) {
						creature.update();
					}
					// creature spells
					ArrayList<SpellDummy> cDummies = creature.getSpellDummies();
					for (int j = 0; j < cDummies.size(); j++) {
						SpellDummy dummy = (SpellDummy) cDummies.get(j);
						if (!dummy.isEffectFinished()) {
							dummy.update();
						}
						else
							cDummies.remove(dummy);
					}
				}
				// update player sprite after move, before collision check
				collisionController.checkCollisions();
			} else {
				// Player is dead
				
			}
		}
	}
	
	public static GameMap getCurrentMap() {
		return maps.get(player.getMapId().ordinal());
	}
	
	public static void changeMap() {
		// Current map = map 1 for now
		currentAreaGameObjects = maps.get(player.getMapId().ordinal()).getAreas().get(player.getArea()).getGameObjects();
		
		collisionController.loadMapGameObjects(getCurrentMap());
	}
	
	public static void changeArea(Unit unit) {
		// Current map = map 1 for now
		currentAreaGameObjects = maps.get(unit.getMapId().ordinal()).getAreas().get(unit.getArea()).getGameObjects();
	}
	
	// No differences with initGame() atm
	public void RestartGame() {
		initGame();
	}
		
	public void keyReleased(KeyEvent e) {
		player.keyReleased(e);
	}
	
	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		
		if (inGame) {
			if (player.isInInventory())
				player.getInventory().keyPressed(e);
			else
				player.keyPressed(e);
		}
		if (gmenu.isInMenu())
			gmenu.keyPressed(e);
		if (gomenu.isInMenu())
			gomenu.keyPressed(e);
		if (smenu.isInMenu())
			smenu.keyPressed(e);
		
		//Game actions
		if (key == KeyEvent.VK_ESCAPE) {
			if (gomenu.isInMenu()) {
				System.exit(0);
			}
			else if (smenu.isInMenu()) {
				smenu.setInMenu(false);
				inGame = true;
			}
			else if (inGame) {
				inGame = false;
				gmenu.setInMenu(true);
			}
			else if (gmenu.isInMenu()) {
				gmenu.setInMenu(false);
				inGame = true;
			}
		}
	}
}
