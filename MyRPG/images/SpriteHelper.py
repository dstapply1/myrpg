from PIL import Image
import glob

# Function Defines

def FlipImage(imgPath):
    try:
        img = Image.open(imgPath)
    except:
        print("Unable to open image: " + imgPath)
    
    img = img.transpose(Image.FLIP_LEFT_RIGHT)
    fileType = imgPath[len(imgPath)-4:len(imgPath)]
    if "right" in imgPath:
        fileName = imgPath.replace("right", "left")
        img.save(fileName)
    elif "left" in imgPath:
        fileName = imgPath.replace("left", "right")
        img.save(fileName)
    else:
        fileName = imgPath.replace(fileType, "_flipped" + fileType)
        img.save(fileName)

def main():
   pngs = glob.glob('./*.png')
   jpgs = glob.glob('./*.jpg')
   images = pngs + jpgs
   if len(images) == 0:
       print("No images found")
   else:
       for i in range(len(images)):
           FlipImage(images[i])

# Run

main()
